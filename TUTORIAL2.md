# Tutorial 2: Automating Build to GitLab CI

## Known Working Version Details

Tested Date: **2024-05-30**

Testing Version (GitLab and Runner): **Enterprise Edition 17.0.0 SaaS**

Tested Hardware: Raspberry Pi 3A+ 512MB (as "Big Pi"), Raspberry Pi 4b 4GB (as "Big Pi"), Raspberry Pi Pico

To report problems, [check the issues here](https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-on-rpi-pico-w) to see if it is already known before creating a new issue in that project.

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be too quick to clean up open tabs.
  **IMPORTANT**: Generally you are asked to close Web IDE tabs immediately after commiting because using old ones causes Merge Conflicts if the code has been updated in another Web IDE tab.

## Prerequisties

This tutorial requires that Tutorial 1 has been completed.

| ![Checklist icon](images/admonitions/list-ul25.png)  Learning Objectives and Concepts |
| ------------------------------------------------------------ |
| - Observe how to re-engineer the development tools into a container <br />- Refactor the build script as a CI Job<br />- Run the build in the cloud<br />- The build is done on the cloud rather than the hardware because we want to maximize the level of virtualization of every part of the pipeline so that we can have unlimited parallel scalability whenever possible to maximize developer productivity.<br />![labsetup](images/objectivesrefactorbuildtoci.png) |

| ![](images/admonitions/lightbulb25.png) Electronic Copy of Workshop Exercises |
| ------------------------------------------------------------ |
| Since there is a lot of code copying from these exercises, you may wish to open them in a dedicated browser window at:  https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-workshop-refactoring-to-ci |

## Exercises

### 2.1 Migrating Build Environment Setup to CI 

You will be comparing the Build Environment Setup code to an GitLab CI/CD Component that has been created to do the same build.

1. Open https://gitlab.com and login with your user id.

2. Tutorial 1.3 contains this code:

   ```
   sudo apt update ;
   sudo apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential python3 git -y ;
   sudo mkdir -p /pico ;
   sudo chown $USER:$USER /pico ;
   cd /pico ;
   git clone https://github.com/raspberrypi/pico-sdk.git --branch master;
   cd pico-sdk ;
   git submodule update --init ;
   echo 'export PICO_SDK_PATH=/pico/pico-sdk' >> ~/.profile ;
   export PICO_SDK_PATH=/pico/pico-sdk ;
   cd /pico ;
   ```

3. On your laptop open https://gitlab.com/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk/

4. **Click** 'Dockerfile' to see this in the top half:

   ```
   FROM debian:bookworm-20240513-slim
   
   RUN apt update && apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential python3 git -y &&\
       rm -rf /var/lib/apt/lists/* &&\
       git clone https://github.com/raspberrypi/pico-sdk.git --branch master &&\
       cd pico-sdk &&\
       git submodule update --init &&\
       apt-get -yqq autoremove && apt-get -yqq autoclean && \
       rm -rf /var/lib/apt/lists/* /tmp/*
   
   ENV PICO_SDK_PATH=/pico-sdk
   ```

5. Up through about the middle of the script, the installation steps are identical, except the Dockerfile script:

   1. has to handle a few security details since we aren't running as root like we are in a Docker build
   2. starts with the smallest possible container that will work (FROM line)
   3. is necessarily obsessive about disk cleanup given how many times the container will be replicated in CI
   4. chains multiple shell script lines together because each "RUN" command creates an intermediary docker disk layer that can be cached and reused anywhere in the container ecosystem that this container is used.

   > You could use this Dockerfile to build a local container and then use that container for a standard build environment like many internet examples do. However, we want to go a step further and run this in GitLab CI.

   | ![DevOps Logo](images/admonitions/devops25.png) CI Automation Engineering: Separating Tools from Builds |
   | ------------------------------------------------------------ |
   | Dockerfile editing is the main skill in porting build tools into GitLab CI. However, building the Dockerfile itself has been automated for you through the GitLab CI/CD Component [Kaniko](https://gitlab.com/explore/catalog/guided-explorations/ci-components/kaniko). You can see where the Arm GNU Embedded Raspberry Pi Pico SDK is built by simply including the Kaniko component [as shown here](https://gitlab.com/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk/-/blob/main/.gitlab-ci.yml?ref_type=heads#L2).<br /><br />By default the container will be built on amd64 or arm64 based on the hardware arch of the GitLab Runner that the Kaniko jobs runs on. However, with [just these 11 additional lines](https://gitlab.com/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk/-/blob/main/.gitlab-ci.yml?ref_type=heads#L12-24) you can configure the Kaniko Component to do a multi-arch build for a container that supports both amd64 or arm64 natively. |

6. In a web browser open https://gitlab.com (you should be logged on)

7. In the *upper left corner* **Find and Click** 'Search or go to...' (a button)

8. In the popup dialog, about 4 lines down, **Click** 'Explore'

9. In the left navigation **Click** 'CI/CD Catalog'

10. You are viewing a catalog of GitLab and community created components that can be used in any GitLab pipeline.

11. In search, **Type** 'arm gnu embedded' and **Press** 'enter'

12. In the list, **Click** 'Arm Gnu Embedded RPI Pico SDK'

    > The Dockerfile we examined earlier builds the container used for this CI/CD component.

13. Read through the 1 page document that appears.

14. Near the top of the document, under the heading, **Find** the tab bar 'Readme | Components'

15. **Click** 'Components'

    1. Notice that you can copy the include code pegged to the latest version.
    2. Notice the Inputs section - these can be customized along with the include lines.

| ![Small Tanuki](images/admonitions/devops25.png) CI Automation Engineering: Why Bother With Containers for CI Builds? |
| ------------------------------------------------------------ |
| For organizations that already have CI working for Embedded, they may be using monolithic build agents that are entire (virtual) machines instead of containers. Generally this results in a brittle, monolithic configuration for the build agent itself - which simply retains the same configuration challenges of completely local development environments. If the build agent image is maintained by hand, it can also result in loss of critical configuration knowledge for the build agent - resulting in the classic problem of it taking a new engineer weeks to successfully perform their first build.<br /><br />Containers add the following substantial benefits for CI:<br />- It forces all build agent knowledge into automation code - which automatically makes the build agent creation process completely documented and explicitly known<br />- Isolation of build runtimes from each other and the rest of the system - supporting multiple runtime dependency versions becomes quite easy<br />- A clean environment for every job<br />- Possibility of leveraging existing, official containers that need just a few quick installs before getting started.<br /><br />**Note:** To achieve all the above benefits a replication of a monolithic Build Agent into a container should be avoided. Or it should be considered an interim step on the way to refactoring to loosely coupled runtime containers for builds. |

### 2.2 Use a GitLab CI/CD Component to Perform Your Build in CI

| ![Small Tanuki](images/admonitions/gitlab-logo-20.png) GitLab Game Changer: GitLab CI/CD Components and Catalog |
| ------------------------------------------------------------ |
| GitLab CI/CD Components and Catalog is a structured way in which teams can create, manage, share and reuse bits of functionality CI/CD code. This does for GitLab CI/CD what dependency management mechanisms like NPM, NuGet and Conan do for the specific languages and frameworks they support.<br /><br />Similar to mature open source dependency managers, the GitLab CI/CD Catalog provides: <br />- semversioning capability to allow version pegging for tested, reliable production-grade dependencies<br />- public and private repositories of managed dependencies<br />- the ability to bundle CI code<br />- dependencie tree capabilities that allow CI/CD Components to depend on other CI/CD Components |

![](images/admonitions/classroom20.png) **For Classrooms**: Please ask your instructor if there is a classroom group level for the classroom. If one is being used, it will take the place of `full/path/to` in the below. The instructor may also have you use a GitLab LearnLabs redemption code to bootstrap into a personal group in the classroom group.

1. Start by creating a new project in the GitLab group of your choosing on the GitLab instance of your choosing. Throughout these exercises this group will be represented as `full/path/to/yourgroup`.

2. Use the GitLab UI to **navigate to** ‘full/path/to/yourgroup’

3. On the left navigation, **Click** {the group name}

4. Near the top right of the page, **Click** ‘New project’ (blue button)

5. On the *Create new project* page, **Click** ‘Import project’ (Large panel)

6. On the *Import project* page, **Click** ‘Repository by URL’ (upper right button)

   > Note: Page expands into the Repository by URL form.

7. Find field *Git repository URL*, **Type or Paste** https://github.com/raspberrypi/pico-examples.git
   Throughout these exercises this project will be represented as `full/path/to/yourgroup/pico-examples`

8. **Click** ‘Create project’ (bottom left button)

   > Note: After the import completes, the Project overview page will display for your new project.

### 2.3 Use Merge Request To Update The Code and Add CI

In this section you will create a GitLab Issue and Merge Request. It is important to work on code using a Merge Request so that we can see all defects associate with just the code we’ve changed on our branch - including security vulnerabilities - which are also ONLY for code we’ve changed or added.

1. While in a browser tab on `full/path/to/yourgroup/pico-examples`

2. **Click** ‘Plan => Issues => New issue’ (button)

3. On *New Issue*, for *Title (required)*, **Type** ‘Updates’

4. **Click** ‘Create issue’

5. On *Updates {new issue view}* in the upper right, **Click** ‘Create merge request’ 

6. On *New merge request*, **Uncheck** ‘Mark as draft’

7. At the bottom of the page, **Click** ‘Create merge request’

8. The new merge request loads as *Resolve “Updates” {new merge request view}*

   | ![Small Tanuki logo](images/admonitions/gitlab-logo-20.png)Merge Requests - Developer’s Single Source of Truth and Collaboration |
   | ------------------------------------------------------------ |
   | GitLab’s work flow from Issue to Merge Request automatically creates a branch to work on. By initiating this workflow from the issue, there is immediate visibility that work has started and all the items are cleanly linked together (Repository, Issue, Merge Request & Branch). The branch and merge request are already associated and the issue will be closed upon successfully merging. The Merge Request is also the Single Source of Truth (SSOT) for all changes and change approvals for the developer and all collaborators and approvers. |

9. In the upper right, **Locate** and **Click** 'Code' (button) and **Select** ‘Open in Web IDE’

   **Note**: A new browser tab opens with VS Code editing a copy of your project.

   > GitLab supports multiple hosted code editing environments. The WebIDE is a VS Code based editor for all your files. GitPod enables developer environments along with the IDE environment. Single file editing is also available for quick changes.

10. In the file navigation of the VS Code Web IDE, **Navigate to and Click** /pico_w/wifi/blink/CmakeLists.txt'

11. Under _target_link_libraries_, right after _pico_stdlib_ **Add** 'pico_stdio_usb', so that the block looks like this:

    ```c
    target_link_libraries(picow_blink
            pico_stdlib              # for core functionality
            pico_stdio_usb
            pico_cyw43_arch_none     # we need Wifi to access the GPIO, but we don't need anything else
            )
    ```

    > Note: This is the same change we made on the repo we cloned to the Big Pi - but now we are doing it on GitLab as our SCM.

    | ![Small Tanuki logo](images/admonitions/devops25.png) CI Automation Engineering: On board code |
    | ------------------------------------------------------------ |
    | The inclusion of pico_stdio_usb is necessary for the Pico to respond to a new boot mode select command. Other RTOS board and OSes will likely have similar requirements if they support firmware update in production via automation. |

12. At the bottom of file add the following lines

    ```c
    pico_enable_stdio_usb(picow_blink 1)
    pico_enable_stdio_uart(picow_blink 0)
    ```

13. You will now change the flash rate of the LED to make it easy to observe that we have new firmware on the device.

14. In the file navigation of the VS Code Web IDE, **Navigate to and Click** /pico_w/wifi/blink/picow_blink.c'

15. At the bottom of the #include list, add this line:

    ```c
    #include <stdio.h>
    ```

16. In the while loop, before the first `sleep_ms` command add `printf("LED ON!\n");` and change the value in `sleep_ms` from 250 to 1000

17.  Before the second `sleep_ms` command add `printf("LED OFF!\n");` and change the value in `sleep_ms` from 250 to 1000, the while loop should now look like this:

    ```
        while (true) {
            cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
            printf("LED ON\n");
            sleep_ms(1000);
            cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
            printf("LED OFF\n");
            sleep_ms(1000);
        }
    ```

18. In the file navigation, collapse the file tree by **Clicking** '/pico_w' (Files save automatically in the Web IDE)

19. In the file navigation, **Click** 'README.md'

    > This simply positions the cursor for the creation of a new file in the proper place.

20. At the top of the file navigation, to the right of PICO_EXAMPLES, **Click** {the add document icon} (a little page with a plus on it)

21. The file name editable text opens, **Type** `.gitlab-ci.yml` (the leading period must be used)

22. ![](images/admonitions/classroom20.png) **For Classrooms**: The instructor may ask you to insert the following code at the top of the file and give you a tag - ask them if this is needed if you were not told. If you are not in a classroom, skip this step:

    ```
    default:
      tags:
        - {a tag the instructor gives you}
    ```

23. In **a NEW** browser tab, open the GitLab CI/CD Catalog (https://gitlab.com/explore/catalog)

24. **Search for and Click** 'Arm GNU Embedded Raspberry Pi Pico SDK' (https://gitlab.com/explore/catalog/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk)

25. Under 'Usage', copy the code fragment that looks like this:

    ````yaml
    include:
      - component: $CI_SERVER_FQDN/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk/rpi-pico-sdk@<VERSION>
        inputs:
          PICOSDK_COMP_CONTAINER_TAG: <VERSION>
    
    pico_build:
      extends: .rpi-pico-sdk
      script:
        - |
          echo "A super cool build for the Pi Pico"
    ````

26. Switch back to the Web IDE browser tab and Paste the code into .gitlab-ci.yml

27. We will also add this `workflow:` block right before pico_build: to help GitLab understand when to create pipelines, the final result should look like this:

    ```yaml
    include:
      - component: $CI_SERVER_FQDN/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk/rpi-pico-sdk@<VERSION>
        inputs:
          PICOSDK_COMP_CONTAINER_TAG: <VERSION>
    
    workflow:
      rules:
        - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG'
          when: never
        - if: $CI_COMMIT_BRANCH
          when: always
          
    pico_build:
      extends: .rpi-pico-sdk
      script:
        - |
          echo "A super cool build for the Pi Pico"
    ```

28. Switch back to the CI/CD Component Page and remember the version number in a blue bubble to the right of the Component Title ('Arm GNU Embedded Raspberry Pi Pico SDK')

29. Switch back to the Web IDE browser tab and replace the **TWO** occurences of '\<VERSION\>' with the version from the component page.

30. Notice this code is very similar to the original commands you used to build the software once you had the tools installed. Make a copy of these code lines:

    ```bash
          mkdir build ;
          cd build ;
          cmake .. -DPICO_BOARD=pico_w ;
          cd pico_w/wifi/blink ;
          make -j4 ;
          ls -l
    ```

31. In the Web IDE tab, replace the below code with the above copy:

    ```
          echo "A super cool build for the Pi Pico"
    ```

    IMPORTANT: All tabbing must be consistent. The result should look like this:
    ```yaml
    pico_build:
      extends: .rpi-pico-sdk
      script:
        - |
          mkdir build
          cd build
          cmake .. -DPICO_BOARD=pico_w
          cd pico_w/wifi/blink
          make -j4
          ls -l
    ```

    | ![Small Tanuki logo](images/admonitions/devops25.png) CI Automation Engineering: Separating Tools from Builds |
    | ------------------------------------------------------------ |
    | In the previous steps we pulled out the build environment creation commands and make them part of a docker file so they could be reused. Now the specific steps to build code are seperated and placed into the CI file that works on the specific code repository. |

    | ![Small microscope logo](images/admonitions/examine-microscope25.png) Note: amd64 and arm64 build execution |
    | ------------------------------------------------------------ |
    | This GitLab CI yaml job is using whatever the default runner architecture is - which on gitlab.com is an amd64 architecture. The underlying container for the **Arm GNU Embedded Raspberry Pi Pico SDK** CI/CD Component is multi-arch and can run on arm64 as well. While many embedded tools run fine on amd64 architecture, if you have builds or tests that cannot run under cross compile or emulation - GitLab runner allows you to run those on Arm runners you have prepared. |

32. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines with a colored number badge)

33. In *Commit message*, **Type** 'Add CI'

34. **Click** ‘Commit to '1-updates'

    > When working in GitLab’s integrated editing environments, creating a commit is also equivalent to doing a Git push operation, which will also trigger CI checks when CI is enabled.

35. **Close** the Web IDE tab.

36. **Click** {the Merge Request tab from which the IDE was launched}

37. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/pico-examples` and **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

38. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

39. **Click** {the icon under ‘Status’} (probably reads either ‘running’, ‘failed’ or ‘passed

    > Note: The Pipeline view opens.

40. **Click** {the browser back button}
    If you cannot click the browser back button, recenter your location by browsing to `full/path/to/yourgroup/pico-examples` and **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

41. Wait for all jobs to complete successfully (Pipeline Status will be ‘Passed’).

42. **Click** the 'pico_build' job.

43. The last line of the job lists the files and if your build worked, you should see about 6 files starting with `pico_blink.uf2` something like this:
    ```bash
    drwxr-xr-x 3 root root   4096 May 31 21:10 CMakeFiles
    -rw-r--r-- 1 root root 154412 May 31 21:10 Makefile
    -rw-r--r-- 1 root root   1158 May 31 21:10 cmake_install.cmake
    -rwxr-xr-x 1 root root 276136 May 31 21:10 picow_blink.bin
    -rw-r--r-- 1 root root 910144 May 31 21:10 picow_blink.dis
    -rwxr-xr-x 1 root root 331716 May 31 21:10 picow_blink.elf
    -rw-r--r-- 1 root root 276417 May 31 21:10 picow_blink.elf.map
    -rw-r--r-- 1 root root 776771 May 31 21:10 picow_blink.hex
    -rw-r--r-- 1 root root 552448 May 31 21:10 picow_blink.uf2
    Cleaning up project directory and file based variables
    Job succeeded
    ```

    > In the next labs you will create jobs to compress the files and create a versioned GitLab release that points to them.

| ![](images/admonitions/embeddeddevops25.png)  Embedded DevOps |
| ------------------------------------------------------------ |
| - Modernized to cloud instances to build software for Embedded.<br />- Modernized to containers for embedded build tools.<br />- Modernized to a Continuous Integration Job over a local manual build. |
