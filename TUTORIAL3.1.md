# Tutorial 3.1: Firmware Packaging, Storage and Release

## Known Working Version Details

Tested Date: **2024-05-30**

Testing Version (GitLab and Runner): **Enterprise Edition 17.0.0 SaaS**

Tested Hardware: Raspberry Pi 3A+ 512MB (as "Big Pi"), Raspberry Pi 4b 4GB (as "Big Pi"), Raspberry Pi Pico

To report problems, [check the issues here](https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-on-rpi-pico-w) to see if it is already known before creating a new issue in that project.

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs. 
  **IMPORTANT**: Generally you are asked to close Web IDE tabs immediately after commiting because using old ones causes Merge Conflicts if the code has been updated in another Web IDE tab.

## Prerequisties

This tutorial requires that Tutorial 1 and 2 have been completed.

| ![Checklist icon](images/admonitions/list-ul25.png)  Learning Objectives and Concepts |
| ------------------------------------------------------------ |
| - Create GitLab Generic Packages to store firmware<br />- Create GitLab Releases to reference firmware storage<br />![labsetup](images/objectivespackagefirmwarerelease.png) |

| ![](images/admonitions/lightbulb25.png) Electronic Copy of Workshop Exercises |
| ------------------------------------------------------------ |
| Since there is a lot of code copying from these exercises, you may wish to open them in a dedicated browser window at:  https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-workshop-refactoring-to-ci |

## Exercises

| ![Small Tanuki](images/admonitions/gitlab-logo-20.png) GitLab Game Changer: CI Automation Engineering: Avoid Storing Binaries in Git and GitLab Releases as To Publish and Archive Firmware Builds |
| ------------------------------------------------------------ |
| The Git storage mechanism is a very poor choice for long term firmware release archiving because Git cannot store diffs of binary files. GitLab provides both Package Registries and Releases which do not store the production ready binary artifacts in the Git storage system.<br /><br />GitLab has two relevant features that we'll use together to create releases. Generic packages allow us to upload arbitrary binary files or archive files. That alone could be sufficient for publishing, but GitLab releases open up additional important features for compliance and they are a more natural place for folks to look for released code. |

### 3.1.1 Create Release for Firmware

The CI Job needs to be updated so that we can capture the artifacts of the last build and create an archive and release that contains the firmware images.We will now use the code editor on the Merge Request so we end up editing our specific branch. If you accidentally click it back in the repository it will load the default branch and none of your changes will be present.

1. Recenter your location by browsing to `full/path/to/yourgroup/pico-examples` 

2. **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

3. In the upper right, **Locate** and **Click** 'Code' (button) and **Select** ‘Open in Web IDE’

   **Note**: A new browser tab opens with VS Code editing a copy of your project.

4. In the file navigation of the VS Code Web IDE, **Navigate to and Click** '.gitlab-ci.yml'

5. We will add a special component that automatically handles versioning for you without the hastle of storing a version number somewhere.

6. In **a NEW** browser tab, open the GitLab CI/CD Catalog (https://gitlab.com/explore/catalog)

7. **Search for and Click** 'GitVersion Ultimate Auto Semversioning' (https://gitlab.com/explore/catalog/guided-explorations/ci-components/ultimate-auto-semversioning)

   > If you scan the documentation you will see that this component simply publishes a version number variable to the pipeline (actually several variables with variations of the number like "major", "minor", etc).  The labs will use the varaible $GitVersion_LegacySemVer. If you read the original docs on SemVersion you can find out that it looks at the Git tags on the main branch to understand what it should create as a version number. Basically "Last version tag + increment the third position once for each commit since then."

8. Under the heading, **Click** 'Components' (tab)

9. Hover the code snippet and to the right, **Clic**k {the clipboard icon}

10. Switch to your Web IDE tab.

11. At the top of your script paste the code and then fix up the include section to look like this (but should use the latest version rather than 0.1.35):

    ```yaml
    include:
      - component: $CI_SERVER_FQDN/guided-explorations/ci-components/ultimate-auto-semversioning/ultimate-auto-semversioning@1.2.5
      - component: $CI_SERVER_FQDN/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk/rpi-pico-sdk@0.1.35
        inputs:
          PICOSDK_COMP_CONTAINER_TAG: 0.1.35
    ```

    > Now we'll always have the next unique SemVersion available in the pipeline

    | ![DevOps Logo](images/admonitions/devops25.png) CI Automation Engineering: Semantic Versioning as a CI Component |
    | ------------------------------------------------------------ |
    | With the simple addition of this component, the Semantic Versioning tool GitVersion will create a version number and store it in a variety of pipeline variables available throughout the rest of the pipeline. The version is automatically determined with no need to store that last used one. It generates non-conflicting version numbers in busy repositories and has many configuration options to match the version incrementing strategy your team already uses. |

12. At the bottom of your .gitlab-ci.yml file, add the following lines so that the firmware file is collected from the job (note that artifact paths are relative to the build directory we started in - not to any directories our script changed into).
    This becomes part of the last job.

    ```
      artifacts:
        paths:
          - build/pico_w/wifi/blink/picow_blink.uf2
    ```

    > `artifacts:` should be at the same indent level as the `script:` keyword before the script.

13. At the bottom of the file, add this job to compress our firmware into a tar archive and add it to GitLab Generic Package Registry. Read the "echo" statements in the code to understand what the sections do.

    ```yaml
    create_generic_package:
      stage: deploy
      image: bash
      before_script:
       - apk add curl
      script:
        - |
          if [[ "${CI_SCRIPT_TRACE}" == "true" ]] || [[ -n "${CI_DEBUG_TRACE}" ]]; then
            echo "Debugging enabled"
            set -xv
          fi
          cd build/pico_w/wifi/blink
          echo "picow_blink.uf2" >> includefiles.lst
          tar -czvf ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz -T includefiles.lst
          echo "Pushing package to registry."  
          curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${GitVersion_LegacySemVer}/${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz
    ```
    
    > Notice that the cd line descends us into the same folder we had on the previous job. The file will be there because the artifact command on the build job grabbed a copy and every subsequent job has the artifacts expanded into the job before the script starts.
    
    | ![Small Tanuki](images/admonitions/gitlab-logo-20.png) GitLab Generic Packages |
    | ------------------------------------------------------------ |
    | GitLab generic packages allow any file type to be uploaded, referenced and retrieved. Since it is not a formal package manager it does not support the concept of a reference to the latest version nor does it support dependencies between uploaded files. However, it can make an excellent storage mechanism for firmware binaries as they can still be versioned and permanently archived. |
    
14. The following job will create a git version tag and a versioned GitLab Release to point to the generic package. Add it to the bottom of your existing code:

    ```yaml
    gitlab_release:
      image: registry.gitlab.com/gitlab-org/release-cli
      stage: release
      script:
        - echo "running release_job for $GitVersion_LegacySemVer"
      release:
        name: 'Release $GitVersion_LegacySemVer'
        description: 'Created using the release-cli $EXTRA_DESCRIPTION'
        tag_name: '$GitVersion_LegacySemVer'
        ref: '$CI_COMMIT_SHA'
        assets:
          links:
            - name: '$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/$CI_PROJECT_NAME/$GitVersion_LegacySemVer/$CI_PROJECT_NAME.$GitVersion_LegacySemVer.tar.gz'
              url: '$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/generic/$CI_PROJECT_NAME/$GitVersion_LegacySemVer/$CI_PROJECT_NAME.$GitVersion_LegacySemVer.tar.gz'
    ```

15. We've added a stage called release, so we'll need to tell GitLab CI we have a non-default stage to run as well as restate the normal defaults. Before `pico_build` add this line by itself:

    ```
    stages: [.pre, build, test, deploy, release, .post]
    ```

16. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines with a colored number badge)

17. In *Commit message*, **Type** 'Add a release'

18. **Click** ‘Commit to '1-updates'

    > When working in GitLab’s integrated editing environments, creating a commit is also equivalent to doing a Git push operation, which will also trigger CI checks when CI is enabled.

19. **Close** the Web IDE tab.

20. **Click** {the Merge Request tab from which the IDE was launched}

21. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/pico-examples`  and **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

22. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

23. **Click** {the pipeline bubble} to expand the details.

24. Once the get_unique_semversion job is complete, **Click** {the job bubble} and notice what version it chose.

25. **Click** {the browser back button}

26. While the pipeline is processing, feel free to click into other jobs to see what they are doing.

27. Wait for all jobs in the pipeline to complete.

28. On the left navigation **Click** 'Deploy => Package Registry'

29. **Click** 'pico-examples'

30. The tar archive is stored with the version number as part of its name. We could also have put the version number on the firmware binary by renaming it right before archiving it.

31. On the left navigation **Click** 'Deploy => Releases'

    1. For the release at the tope of the page, under *Other*, notice the name and the link point to the package registry file.

> You have completed a release of the firmware


| ![Small Tanuki](images/admonitions/gitlab-logo-20.png) GitLab Game Changer: Getting Binaries Out of Git and Releases to Customers |
| ------------------------------------------------------------ |
| GitLab Packages and Releases allows long term archive and customer access to your binary firmware releases while keeping the binaries out of Git which was never intended to store binaries. Since Git does not diff binary files, the full copy of every version of a binary is stored. If this mechanism is not suitable for you, GitLab has example code that uses NPM to package firmware binaries. |

### 3.1.2 Flash New Firmware

1. On the left navigation **Click** 'Settings => General'

2. Next to *Visibility, project features, permissions*, **Click** 'Expand'

3. In the long list of toggles, **Find** 'Package registry'

4. Immediately below that line, on the indented toggle *Allow anyone to pull from Package Registry*, **Click** {the toggle button} to be enabled.

5. **IMPORTANT**: At the bottom of the long list of toggles, **Find** *Save changes* and **Click** 'Save changes'

6. On the left navigation **Click** 'Deploy => Releases'

7. For the release at the top of the page, under *Other*, **Right Click** {the firmware package url}. and **Select** 'Copy Link Address'

8. SSH into your Big Pi.

9. Change to the home directory with `cd ~`

10. **Type** 'curl -O ' {Capital O, not zero) and paste your url for something that looks like this (url will be different):

   ```
   curl -O https://gitlab.com/api/v4/projects/58387288/packages/generic/pico-examples/0.1.147-1-updates21/pico-examples.0.1.147-1-updates21.tar.gz
   ```

11. **Press** {enter} 

12. Expand the archive with this command `tar -xvf *.tar.gz`

13. The output should show the file picow_blink.uf2 was extracted

14. Unplug the usb for the Pico

15. Hold down the bootsel button on the Pico and plug it into the USB-Micro-B cable end.

15. Find which device the Pico attaches as with `sudo lsblk`
    Look for a lines like the below

    ```NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
    sda           8:0    1  128M  0 disk
    └─sda1        8:1    1  128M  0 part
    mmcblk0     179:0    0 29.7G  0 disk
    ├─mmcblk0p1 179:1    0  512M  0 part /boot/firmware
    └─mmcblk0p2 179:2    0 29.2G  0 part /
    ```

    **In the above example we know the device is sda1 - your device may be different**
    
17. Make a mount directory with ` sudo mkdir -p /mnt/pico`

18. Mount the device to the directory with (but substitute your actual found device first) `sudo mount /dev/sda1 /mnt/pico`

    > Ignore fstab warnings.

19. Check that we are actually mounted with (observe the two files)

    ```
    $ ls /mnt/pico/
    INDEX.HTM INFO_UF2.TXT
    ```

20. Copy the firmware to the mounted Pico with

    ```
    sudo cp picow_blink.uf2 /mnt/pico ;
    sudo sync
    ```

21. If everything was successful, the LED on the Pico should have a much slower blink cycle.

| ![Checklist icon](images/admonitions/success-check-circle25.png)  Accomplished Outcomes |
| ------------------------------------------------------------ |
| - Created GitLab Generic Packages to store firmware<br />- Created GitLab Releases to reference firmware storage |
