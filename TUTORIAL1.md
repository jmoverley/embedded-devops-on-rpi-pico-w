# Tutorial 1: Manual Build: Blinking Light On Raspberry Pi W

## Known Working Version Details

Tested Date: **2024-05-30**

Tested Version (GitLab and Runner): **Enterprise Edition 17.0.0 SaaS**

Tested Hardware: Raspberry Pi 3A+ 512MB (as "Big Pi"), Raspberry Pi 4b 4GB (as "Big Pi"), Raspberry Pi Pico

Wait Times in exercises are for the RPI 3A+.

The Big Pi is only ever used to build software for Tutorial 1 - so spending extra on a larger RPI only benefits Tutorial 1.

To report problems, [check the issues here](https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-on-rpi-pico-w) to see if it is already known before creating a new issue in that project.

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs. 
  **IMPORTANT**: Generally you are asked to close Web IDE tabs immediately after commiting because using old ones causes Merge Conflicts if the code has been updated in another Web IDE tab.
- Since the two hardware devices are from the same company, exercises will be referring to them as "Big Pi" and "Pi Pico"
- ![Small Tanuki](images/admonitions/classroom20.png) **For Classrooms** = indicates special instructions for when these materials are being used in an instructor-led classroom.

| ![Checklist icon](images/admonitions/list-ul25.png)  Learning Objectives and Concepts |
| ------------------------------------------------------------ |
| - You will simply be doing a manual project by hand to set a baseline of a manual embedded development workflow. <br />- The exercises use C language rather than on-chip hobby languages and frameworks.<br />- Our development environment is done on a Posix capable board because, for embedded HIL, GitLab Runner generally needs to run in a gateway architecture on at least a posix OS capable board. So the transition of the development environment to become a GitLab Runner Gateway later will be an easy to consider mental step.<br /> ![labsetup](images/objectivesmanualdevenv.png) |

| ![](images/admonitions/lightbulb25.png)  Electronic Copy of Workshop Exercises |
| ------------------------------------------------------------ |
| Since there is a lot of code copying from these exercises, you may wish to open them in a dedicated browser window at:  https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-workshop-refactoring-to-ci |

## Exercises

You must complete [TUTORIAL1-PREP.md](TUTORIAL1-PREP.md) before continuing

### 1.1 Preparing the Development Environment on the Big Pi

Hands on: 1m, wait: 15m (for Pi 3A+)

1. While logged onto the Big Pi, run the following shell commands (they can be copied and pasted in one block):

   ```bash
   # Install packages for building Arm software
   sudo apt update ;
   sudo apt install cmake gcc-arm-none-eabi libnewlib-arm-none-eabi build-essential python3 git libusb-1.0-0-dev pkg-config -y ;
   # Clone the pico-sdk into a directory
   sudo mkdir -p /pico ;
   sudo chown $USER:$USER /pico ;
   cd /pico ;
   git clone https://github.com/raspberrypi/pico-sdk.git --branch master;
   cd pico-sdk ;
   # Ensure any nested Git repositories are cloned as well
   git submodule update --init ;
   # Ensure the Sdk is on the path
   echo 'export PICO_SDK_PATH=/pico/pico-sdk' >> ~/.profile ;
   export PICO_SDK_PATH=/pico/pico-sdk ;
   cd /pico ;
   ```
It may take between 7 and 15 minutes to process all of these commands as there are OS package installs and Git clone commands.

### 1.2 Clone and Build Blink Firmware

Hands on: 5m, Wait: 3.5m

Undervoltage in this lab may cause the device to become unresponsive with no error.

1. Clone the pico examples and prepare them 

   ````bash
   # Clone the pico examples project
   cd /pico ;
   git clone https://github.com/raspberrypi/pico-examples.git --branch master /pico/pico-examples ;
   mkdir -p /pico/pico-examples/build ;
   cd /pico/pico-examples/build ;
   ````

2. Edit the CMakeList.txt file to include an additional library 

   ```bash
   nano /pico/pico-examples/pico_w/wifi/blink/CMakeLists.txt
   ```

3. Under _target_link_libraries_, right after _pico_stdlib_ Add 'pico_stdio_usb', so that the block looks like this (use spaces not tabs):

   ```c
   target_link_libraries(picow_blink
           pico_stdlib              # for core functionality
           pico_stdio_usb
           pico_cyw43_arch_none     # we need Wifi to access the GPIO, but we don't need anything else
           )
   ```

   > Note: This addition will allow us to put the Pi Pico W in boot select mode with a CLI command.

4. At the bottom of file add the following lines

   ```c
   pico_enable_stdio_usb(picow_blink 1)
   pico_enable_stdio_uart(picow_blink 0)
   ```

5. Save and exit by **Pressing** 'CTRL-X' and then 'y'

6. Edit the CMakeList.txt file to include an additional library

   ```bash
   nano /pico/pico-examples/pico_w/wifi/blink/picow_blink.c
   ```

7. At the bottom of the #include list, add this line:

   ```c
   #include <stdio.h>
   ```

8. In the while loop, before the first `sleep_ms` command add `printf("LED ON!\n");

9. Before the second `sleep_ms` command add `printf("LED OFF!\n");`  the while loop should now look like this:

   ```
       while (true) {
           cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 1);
           printf("LED ON\n");
           sleep_ms(250);
           cyw43_arch_gpio_put(CYW43_WL_GPIO_LED_PIN, 0);
           printf("LED OFF\n");
           sleep_ms(250);
       }
   ```

10. Save and exit by **Pressing** 'CTRL-X' and then 'y'

11. Edit a file in the SDK (so that it produces a fully functional copy of picotool)

    ```bash
    nano /pico/pico-sdk/tools/Findpicotool.cmake
    ```

12. To find a string press CTRL + W

13. Type ''LIBUSB" (uppercase)

14. You should find yourself on a code line like this:

    ```
    "-DPICOTOOL_NO_LIBUSB=1"
    ```

15. Update the line to set the value to zero like this:

    ```
    "-DPICOTOOL_NO_LIBUSB=0"
    ```

16. Save and exit by **Pressing** 'CTRL-X' and then 'y'

17. Build the project with

   ```bash
     cd /pico/pico-examples/build ;
     cmake .. -DPICO_BOARD=pico_w ;
     cd pico_w/wifi/blink ;
     make -j4
   ```

11. Examine built binaries with `ls -l picow_blink*`, you should see ten files with names that begin with picow_blink.*

12. Move the picotool command to a place where it can be accessed machine wide with:

    ```
    sudo cp /pico/pico-examples/build/_deps/picotool/picotool /usr/sbin
    ```

13. Validate the pico tool location and build with:

    ```
    picotool help
    ```

14. In the output, under COMMANDS, there should be a line for "reboot". If it is missing the build did not occur correctly and should be redone.

### 1.3 Flash the Firmware to the Pico

1. Plug one end of the USB A to USB-Micro-B cable into an USB port on the Big Pi.

2. Hold down the bootsel button on the Pico and plug it into the USB-Micro-B cable end.

   NB. Keep holding the button for a few seconds before moving to the next step.

3. Confirm the Pico device is connected and in boot mode with `lsusb`
   ```
   Bus 001 Device 005: ID 2e8a:000a Raspberry Pi Pico Boot
   Bus 001 Device 003: ID 0424:ec00 Microchip Technology, Inc. (formerly SMSC) SMSC9512/9514 Fast Ethernet Adapter
   Bus 001 Device 002: ID 0424:9514 Microchip Technology, Inc. (formerly SMSC) SMC9514 Hub
   Bus 001 Device 001: ID 1d6b:0002 Linux Foundation 2.0 root hub
   ```

   NB. you will see `Raspberry Pi Pico Boot` - if the device is not in bootsel mode you will see simply see `Raspberry Pi Pico`

4. Find which device the Pico attaches as with `sudo lsblk`
   Look for a lines like the below
   
   ```NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
   sda           8:0    1  128M  0 disk
   └─sda1        8:1    1  128M  0 part
   mmcblk0     179:0    0 29.7G  0 disk
   ├─mmcblk0p1 179:1    0  512M  0 part /boot/firmware
   └─mmcblk0p2 179:2    0 29.2G  0 part /
   ```

   From this we know the device is sda1.

4. Make a mount directory with ` sudo mkdir -p /mnt/pico`

5. Mount the device to the directory with (but substitute your actual found device first) `sudo mount /dev/sda1 /mnt/pico`

6. Check that we are actually mounted with the command (and observe the two files that list immediately after in order to verify connection)
   ```
   $ ls /mnt/pico/
   INDEX.HTM INFO_UF2.TXT
   ```

7. Copy the firmware to the mounted Pico with

   ```
   cd /pico/pico-examples/build/pico_w/wifi/blink ;
   sudo cp picow_blink.uf2 /mnt/pico ;
   sudo sync
   ```
   ![Checklist icon](images/admonitions/examine-microscope25.png) build directory was : /pico/pico-examples/build/pico_w/wifi/blink/

8. After a few seconds the light on the Pico should start flashing.

9. As the pico has now restarted and is running your new firmware, we have to just tidyup and umount the device (which is no longer available). 
   ```
   sudo umount /mnt/pico
   ```

| ![Checklist icon](images/admonitions/success-check-circle25.png)  Accomplished Outcomes |
| ------------------------------------------------------------ |
| - Completed manual embedded project by hand<br />- Used Big Pi as developer environment. |
