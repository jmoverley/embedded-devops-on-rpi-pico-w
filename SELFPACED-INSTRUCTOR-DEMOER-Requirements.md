# Table of Contents

[TOC]

# Prerequisities

**IMPORTANT:** These requirements are stated for a self-paced learner working alone. There is a special section for classroom delivery after these requirements tables.

Keeping Hardware Requirements Low and Avoiding Conflicts on Participant Laptops

- Networking is configured with either DHCP for self-paced learners or assigned IP addresses for classrooms to avoid the complexity and cost of needing a hardware keyboard and monitor to get going on the Big Pi.
- Development Environments are only configured on the Big Pi and in GitLab - avoiding challenges of conflicts on participant provided laptops - such as corporate security policies and pre-existing development environments that either conflict or must remain intact.

## Prerequisite Hardware

**GitLab Team Members** - expensing is limited to $100 USD - which can outfit you nicely [if you follow these ordering and expensing guidelines](#expensing-guidelines-for-gitlab-team-members).

| Item                                                         | How To Obtain It                                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| 1x Raspberry PI 3A+ with Wifi (tested as minimum working edition)(with appropriate Power Supply and USB Power Cable)<br /><br />Future Software Defined Vehicle Tutorials require a PI with about 4GB - so the Pi 4 with 4GB would be a future proof option.<br /><br />Raspberry Pi 3 A+ has a full size HDMI port - so if you cannot look at your router to see your DHCP assigned address, this model can make attaching to an existing monitor easier that the Pi 4 or higher that require special "Micro HDMI" port adapters. | Raspberry Pi 3 A+ 512MB (USB-micro-b powered) [$25 USD at Mouser](https://mou.sr/47VprKh)<br />Raspberry Pi 4 B with 2GB (USB-C powered) [$45 at Mouser](https://mou.sr/480mUxA)<br />Raspberry Pi 4 with 4GB (USB-C powered): [$55 at Mouser](https://eu.mouser.com/ProductDetail/Raspberry-Pi/SC1111?qs=HoCaDK9Nz5fnLhlMNnKTiQ%3D%3D)<br />Raspberry Pi 5 with 4GB (USB-C powered): [$60 at Mouser](https://www.mouser.com/ProductDetail/Raspberry-Pi/SC1111?qs=HoCaDK9Nz5fnLhlMNnKTiQ%3D%3D) <br />Simply Change Country at Mouser for Your Region. <br />[Raspberry Pi's Reseller List by Country](https://www.raspberrypi.com/resellers/?q=&country=1). |
| 1x Raspberry PI Pico W (**be sure to get the "W" version**) No presoldered headers are needed - though you may find the useful for other projects. | Raspberry Pi Pico W (USB-Micro-b power and data) [$7 USD at Mouser](https://mou.sr/3ViDZxY). <br />Simply Change Country at Mouser for Your Region. <br />[Raspberry Pi's Reseller List by Country](https://www.raspberrypi.com/resellers/?q=&country=1). |
| USB Power. 5V, 2.5A for PI 3A+, 5V, 3.0A for Pi 4 & 5        | RPI 3 Power Supply 5V, 2.5A USB Micro-b $8 USD ([CED](https://chicagodist.com/products/raspberry-pi-2-amp-micro-usb-power-supply), [Adafruit](https://www.adafruit.com/product/1995))<br />RPI 4 Power Supply 5V, 3.0A USB-C: [$8 USD at Mouser](https://mou.sr/3RijosA)<br /><br />RPI 5 Power Supply 5V, 5A USB-C: [$12 USD at Mouser](https://www.mouser.com/ProductDetail/Raspberry-Pi/SC1153)<br />USB Chargers are NOT Power Supplies  (even though they appear to work) because:<br />- they are frequently limited to 2.4A<br />- they generally have intelligent charging negotiation protocols (e.g. QC 3.0) which only supply higher amps if negotiated and usually fall back to 5V 2.4A if a device does not negotiate a higher rate (to be safe).<br />- they do not have to maintain a constant voltage nor amperage that a power supply gives. They can fluxuate both parameters and frequently don't maintain stated voltage under high current draw.<br /><br />Under voltage can be detected by installing Raspberry Pi OS and after boot running `dmesg` and looking for the most *recent* occurence of "Voltage normalized". If it is not followed by "Undervoltage detected!" you have sufficient power.<br /><br />A sign of undervoltage in these labs is a silent system crash when building the project (looks like it is taking forever) |
| Male USB A (2.0 or 3.0 are fine) to Male USB-micro-b Data Capable Cable - to tether the two PIs together for both power and data. | [USB connector guide](https://www.cablestogo.com/learning/connector-guides/usb)<br />$3 USD (USA: [CED](https://chicagodist.com/products/usb-cable-a-microb), [Adafruit](https://www.adafruit.com/product/592)) |
| 16GB MicroSD Card                                            | Don't get something overly slow. For physical classrooms, this might be provided for you. |
| Laptop MicroSD Port                                          | Some machines might require a docking station or a Micro to Standard SD adapter in order to access the MicroSD card. Classroom delivery might provide this for you. |
| A Computer with SSH configured and a Micro SD Port<br />**Note:** no changes will need to be done to the laptop and the operating system does not matter as the Web IDE is how all code editing occurs in these workshops. | All project changes are done in the Web IDE and through SSH so that no local configuration of editors or runtimes is required. |
| A Wireless Network including SSID and password and ability to lookup DHCP assigned addresses.<br /><br />The Raspberry Pis connecting to the Wireless network will need to be able to access the Internet.<br /><br />The Raspberry Pi 4 does have a physical network port if that is helpful for providing networking. | This is required to SSH to the Raspberry Pis during class.<br /><br />You will need to either be able to figure out what DHCP address was assigned to your Pi or manually assign an IPs.<br />Alternatively you could connect a keyboard and monitor - special Micro HDMI connectors might be required for your chosen Pi.<br />Note: The Pi 3A+ has a full sized HDMI port. |

## Prerequisite Resources for All Tutorials

| Item                                                         | How To Create It                                             |
| ------------------------------------------------------------ | ------------------------------------------------------------ |
| A GitLab User ID on GitLab.com                               | [GitLab.com Sign Up Page](https://gitlab.com/users/sign_up)<br />(email confirmation required) |
| A GitLab.com Namespace Organizational Tenant with Ultimate license (New group at root of GitLab.com) | Once logged into GitLab.com, simply create a new unique group name as a root group, such as https://gitlab.com/mygroup555. In the group, go to “Billing” and enable a 30 day Ultimate trial.<br />It is important to have access to Ultimate features - so only choose a self-managed instance if your organization is licensed for GitLab Ultimate.<br />**For instructor-led courses**, the instructor may provide this. |
| A Computer with SSH configured and a Micro SD Port<br />**Note:** no changes will need to be done to the laptop and the operating system does not matter as the Web IDE is how all code editing occurs in these workshops. | All project changes are done in the Web IDE and through SSH so that no local configuration of editors or runtimes is required. |
| Access to Public Projects in https://gitlab.com/guided-explorations/aws/ (or exports of these projects if air gapped) | Only a concern if you are attempting to use a private GitLab Instance. Using a new organizational tenant on GitLab.com allows enablement of a 30 day ultimate trial. |
| A GitLab Runner (Easy Button Setup Provided)                 | Only a concern if you are attempting to use a private GitLab Instance. Using a new organizational tenant on GitLab.com gives free runner minutes with credit card activation.<br />**For instructor-led courses**, the instructor may provide this. |

## Expensing Guidelines For GitLab Team Members

If you complete these exercises as part of a GitLab sponsored event or as a self-paced exercise to be audible, demo or instructor ready, you can expense up to $100 USD for the following equipment bundle.

| Price (USD) | Item                                                         |
| ----------: | ------------------------------------------------------------ |
|         $55 | **Recommended:** Raspberry Pi 4 with 4GB (Bare, no kit) $55 (preferred due to extensive direct ecosystem support in labs, SDKs, etc, e.g. Wind River SDK)<br />This option comes with the "boring technology benefits" of lots of existing ecosystem support for the board and cheaper, more common power supply requirements.<br />*Do not assume that materials for RPI 4 work with RPI 5 - embedded development is highly chipset-specific and so RPI 4 and 5 require different binaries. |
|      or $60 | **Alternative:** Raspberry Pi 5 4GB (Bare, no kit) $60 (faster, newer, less existing support)<br />*Do not assume that materials for RPI 4 work with RPI 5 - embedded development is highly chipset-specific and so RPI 4 and 5 require different binaries. |
|          $7 | Raspberry Pi Pico W  (**EXACT BOARD REQUIRED** - including that it must be the "W" version) <br />(Bare or pre-soldered headers might be nice future-proofing for mounting to a breadboard for about $1 more) |
|          $8 | RPI 4 Power Supply 5V, 3.0A USB-C (Or TEST existing usb power solution - do not underpower a Pi)<br />Alternative: RPI 5 Power Supply 5V, 5A USB-C<br /><br />USB Chargers are NOT Power Supplies (even though they appear to work) because:<br />- they are frequently limited to 2.4A<br />- they generally have intelligent charging negotiation protocols (e.g. QC 3.0) which only supply higher amps if negotiated and usually fall back to 5V 2.4A if a device does not negotiate a higher rate (to be safe).<br />- they do not have to maintain a constant voltage nor amperage that a power supply gives. They can fluxuate both parameters and frequently don't maintain stated voltage under high current draw. |
|          $3 | Male USB A (2.0 or 3.0 are fine) to Male USB-micro-b (**IMPORTANT:** Data Capable Cable) |
|         $10 | 16 GB / 32 GB MicroSD Card (Class 10 - 100Mbps, eg. Samsung Pro, Pro Endurance or EVO/EVO Plus, Sandisk Ultra, Gigastone High Endurance)<br />If within total expense price, High Endurance / Industrial options are nice, but not necessary.<br />Be sure you have a way to plug the card into your laptop - docking station or full-size SD adapter. |
|         $12 | Shipping                                                     |
|    **$100** | **Total** (USD)                                              |

**Expensing Details**

- You will need to cover any amounts over $100 spent on upgrading the above list with Kits, more memory or a higher board level.
- If you cannot get the basics for $100 in your area, please talk with your manager for an exception.
- Global purchasing can be done from the Mouser links below or you can consult [Raspberry Pi's Reseller List by Country](https://www.raspberrypi.com/resellers/?q=&country=1). 
- Please note that Amazon.com USA prices are substantially higher - this might be true of other GEOs as well.
- **For the expense report:**
  - Use the category "Office Supplies"
  - Include a copy of your receipt
  - Include a screenshot of your manager's approval from slack, email, etc. It must clearly mention that the approval is for the course "**Embedded DevOps Workshop - Refactoring to CI and Modern Security**".


# Special Considerations for Classroom Environments

### Networking

Students must use unique PI devices and not accidentally login to someone else's.

This can be accomodated without knowing the IP addresses IF the network in question will properly resolve .local address where a pi host named "mypi25" will be name resolvable at `mypi25.local` This should be tested and never assumed as wireless networking may be isolated from the wired participant computers or network security settings might prevent .local name resolution.

Is situations where .local names are not resolvable or other challenges of device ip address identification and high levels of corporate Wifi and network security, it may be best to obtain permission to setup a wireless router that is gatewayed to the internet. If all devices - pis and workstations use the same router, then .local name resolution is more likely.

Another additional measure to ensure unique, quick network access to each pi is manual address assignment. 

Unique assignment of hostname, username and passwords can also help prevent legitimate accidents in logging into another participants device.

### SD Cards

If all Pi hardware is standard, preflashing and providing cards is adviseable. It not only saves a lot of time, but the appropriate network settings and hostnames can be set for participants. It also saves challenges of flashing incorrect drives. The wireless password is also secured via the RPI flashing utility. This is more important if there is not a dedicated router and participants should not know the WiFi credentials.

[Here is a script](setupforclassroom.md) for configuring a set of SD cards in this way. Since the Raspberry Pi imager does not allow configuration of static IP, the script can be used to prepare just one card as well.

### Preparation

**Caution!** The possibility of logging into another participants Pi without knowing it will be extremely high if all usernames and passwords are the same.

1. Flash each SD card with:
   1.  The WiFi SSID
   2. A unique manual IP4 address (Class C to keep it simple for students)
   3. A UNIQUE username (that includes the last segment of the IP4 address)
   4. A UNIQUE password  (that includes the last segment of the IP4 address)
   5. A UNIQUE hostname (that includes the last segment of the IP4 address)
2. Label each card with the last IP4 address segment.
3. When students boot up the Pi they will know exactly which IP address to SSH to and the pattern of their username and password will also include the unique IP4 segment.
