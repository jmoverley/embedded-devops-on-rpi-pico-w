# Tutorial 5.2: Security Policy Merge Approvals In Action with GitLab SAST

## Known Working Version Details

Tested Date: **2024-06-12**

Testing Version (GitLab and Runner): **Enterprise Edition 17.0.0 SaaS**

To report problems, [check the issues here](https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-on-rpi-pico-w) to see if it is already known before creating a new issue in that project.

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
  **IMPORTANT**: Generally you are asked to close Web IDE tabs immediately after commiting because using old ones causes Merge Conflicts if the code has been updated in another Web IDE tab.

## Prerequisties

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** 

- General Requirements for self-paced execution of this tutorial are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)
- This tutorial requires that Tutorials 1 through 5.1 must also be completed and the resultant project available to start this tutorial.

[Authored as Open Educational Resources (OER) and with Hyperscaling Enablement Content Architecture (HECA)](OPEN-EDUCATION-RESOURCES-HECA.md)

| ![Checklist icon](images/admonitions/list-ul25.png)  Learning Objectives and Concepts |
| ------------------------------------------------------------ |
| - GitLab allows automated blocking and unblocking of Merge Requests based on configurable policy in regard to new vulnerabilities being introduced in the Merge Request. |

![](images/admonitions/lab-answers25.png) **Lab Answers:** [labsections2-5.2gitlab-ci.yml](../../solutions/labsections2-5.2gitlab-ci.yml)

## Exercises

### 5.2.1 See Security Policy Merge Approvals Block a New Vulnerability During Development

1. While in a browser tab on `full/path/to/yourgroup/pico-examples`

2. **Click** ‘Plan => Issues => New issue’ (button)

3. On *New Issue*, for *Title (required)*, **Type** ‘New Feature’

4. **Click** ‘Create issue’

5. On *Updates {new issue view}*, **Click** ‘Create merge request’ 

6. On *New merge request*, **Uncheck** ‘Mark as draft’

7. **Click** ‘Create merge request’

8. On *Resolve “"New Feature" {new merge request view}*

9. Find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

10. On the left file navigation, **Navigate to** ‘pico_w/wifi/blink’

11. **Click** 'picow_blink.c'

12. Right after `int main() {` and before `stdio_init_all();` insert this code:

    ```c
        #define LED_PARAM "led=%d"
        int led_state = 1;
        char params[10] = "1,2,3";
        int led_param = sscanf(params, LED_PARAM, &led_state);
        printf("LED PARAM is %d\n", led_param);
    
    ```

    The result should look like this (just the line before and the line after from the existing code are depicted below):
    ```c
    int main() {
        #define LED_PARAM "led=%d"
        int led_state = 1;
        char params[10] = "1,2,3";
        int led_param = sscanf(params, LED_PARAM, &led_state);
        printf("LED PARAM is %d\n", led_param);
        stdio_init_all();
    ```

13. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

14. In *Commit message*, **Type** ‘New Feature’

15. **Click** the button ‘Commit to ’"{number}-new-feature"‘

16. **Close** the Web IDE tab.

17. **Click** {the Merge Request tab from which the IDE was launched}

18. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/pico-examples` and **Click** ‘Code => Merge requests => Resolve “"New Feature" ’ 

19. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

20. Wait For all jobs to complete successfully - refresh the page as needed. (Pipeline Status should be ‘Passed’).

### 5.2.2 Examining Security Policy Merge Approval Enforcement

1. **Click** 'Overview' (MR Tab)

   The following screenshot can be used to understand the next steps in examining the MR findings and approval status.

![mrviewwithsecuritypolicyblock.png](images/mrviewwithsecuritypolicyblock.png)

2. Notice that our security policy is triggered by the presence of 1 or more Critical or High findings for SAST - this is indicated by the MR line *Requires 1 approval from New High Findings*

2. Next to *Security scanning detected…* , **Click** {the section expand down arrow}

3. Notice the finding is only for the code we just changed. All previous findings that were merged are still available in the Security Dashboard - but the MR View focuses on findings created by the code in the current MR.

4. In the *vulnerability list*, **Click** 'sscanf() functions may allow format string based overflows' for a screen like this:

   ![findingview.png](images/findingview.png)

5. Notice that there are many ways for you to learn about the vulnerability and to collaborate on resolving it.

   | ![](images/admonitions/gitlab-logo-20.png) **[Game Changer]** Right Information, Right Person, Right Time |
   | ------------------------------------------------------------ |
   | <br />Game Changers:<br />- When you learn that you accidentally added a vulnerability through your own coding or a dependency change, you are immediately alerted and you know that it was your code changes. <br />- If you work to eliminate the vulnerability, the rule automatically becomes optional with no human intervention. <br />- Since security can block merging of the code, they do not need to get involved while you remediate it. <br />- Since your pipeline does not fail, you can keep working on your functional code if you need to collaborate to eliminate the vulnerability.<br />- If you need the vulnerability to be accepted because it isn't relevant to your code paths, you can attempt to dismiss it and security can come in and approve the merge if they are satisfied with the rationale. |


### 5.2.3 Removing the Code To See The Security Policy Automatically Become Optional

1. While in a browser tab on `full/path/to/yourgroup/pico-examples`, **Click** ‘Code => Merge requests => Resolve “"New Feature" ’  (or switch to the tab where this is already open)

3. On *Resolve “"New Feature" {new merge request view}*

4. Find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

5. On the left file navigation, **Navigate to** ‘pico_w/wifi/blink’

6. **Click** 'picow_blink.c'

7. Remove the inserted code between `int main() {` and  `stdio_init_all();` 
   The result should look like this (just the line before and the line after the removed code are depicted below):

   ```c
   int main() {
       stdio_init_all();
   ```

8. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

9. In *Commit message*, **Type** ‘Fix vulnerability’

10. **Click** the button ‘Commit to ’"{number}-new-feature"‘

11. **Close** the Web IDE tab.

12. **Click** {the Merge Request tab from which the IDE was launched}

13. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/pico-examples` and **Click** ‘Code => Merge requests => Resolve “"New Feature" ’ 

14. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

15. Wait For all jobs to complete successfully - refresh the page as needed. (Pipeline Status should be ‘Passed’).

16. **Click** 'Overview' (MR Tab)

    The following screenshot can be used to understand the next steps in examining the MR findings and approval status.

    ![](images/mrpolicyunblocked.png)

17. Notice:

    1. MR Approval 'New High Findings' is now optional
    2. The MR is indicated as ready to merge.
    3. The Security Bot shows violations are resolved (also emailed as build status)


| ![](images/admonitions/gitlab-logo-20.png) **[Game Changer]** Voluntary Developer Initiated Remediation Management |
| ------------------------------------------------------------ |
| When developers have:<br />- Timely vulnerability information<br />- That relates to only the code they've been changing<br />- With clear personal responsibility<br />- and helps like just in time training and collaboration<br />- process automation which means no unnecessary human-led steps (policy enforcement)<br /><br />They are very likely to resolve software defects because they want to create great, defect-free code. |

18. **Click** 'Merge'
18. **Ensure** the Merge Request Pipeline completes successfully.

​    
