# Tutorial 5.3: Security Policy Merge Approvals with CodeSonar SAST

## Known Working Version Details

Tested Date: **2024-06-18**

Testing Version (GitLab and Runner): **Enterprise Edition 17.0.0 SaaS**

To report problems, [check the issues here](https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-on-rpi-pico-w) to see if it is already known before creating a new issue in that project.

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
  **IMPORTANT**: Generally you are asked to close Web IDE tabs immediately after commiting because using old ones causes Merge Conflicts if the code has been updated in another Web IDE tab.

## Prerequisties

+ There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** 
- This Tutorial uses [CodeSonar](https://codesecure.com/our-products/codesonar/) by [CodeSecure](https://www.codesecure.com/) and requires a UserID and Password to a licensed CodeSonar instance. 
  + If you are in a classroom environment, the instructor may provide this information.
  + For self-paced delivery, please contact support@codesecure.com to request access.
- General Requirements for self-paced execution of this tutorial are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)
- This tutorial requires that Tutorials 1 through 5.1 must also be completed and the resultant project available to start this tutorial.
- This tutorial starts from the output of Tutorial 5.1

[Authored as Open Educational Resources (OER) and with Hyperscaling Enablement Content Architecture (HECA)](OPEN-EDUCATION-RESOURCES-HECA.md). Includes modifications provided by CodeSecure, Inc.

| ![Checklist icon](images/admonitions/list-ul25.png)  Learning Objectives and Concepts |
| ------------------------------------------------------------ |
| <ul><li> GitLab allows automated blocking and unblocking of Merge Requests based on configurable policy in regard to new vulnerabilities being introduced in the Merge Request - including by third party scanners. <br /><li>In this Tutorial we  combine GitLab SAST which wil highlight dangerous code patterns with CodeSonar for deep defect detection through abstract execution and support for MISRA. </ul> |

![](images/admonitions/lab-answers25.png) **Lab Answers:** [labsections2-5.3gitlab-ci.yml](../../solutions/labsections2-5.3gitlab-ci.yml)

## Exercises

### 5.3.1 Adding CodeSonar Security Scanning Component to Merge Request

1. Recenter your location by browsing to `full/path/to/yourgroup/pico-examples` 

2. In a new browser tab, go to https://gitlab.com/codesonar/components/codesonar-ci. This is a re-usable CI/CD Component that we will include into our project. Feel free to browse the documentation and when done, you can close this tab.

3. ![](images/admonitions/classroom20.png) **For Classrooms**: The instructor will most likely set these variables up for the entire classroom in an upbound group for the entire class, please ask them if that is what is being done.

   The `codesonar-ci` component depends on several CI/CD Variables to be set for username, password and URL of the CodeSonar hub. To do that, go to 
   the left hand navigation in GitLab, **Click** `Settings => CI/CD` and then **Click** `Expand` for `Variables` and create variables for

   + CSONAR_HUB_PASSWORD (Select "Masked", **IMPORTANT: Deselect "Protected variable"**)

   + CSONAR_HUB_USER (**IMPORTANT: Deselect "Protected variable"**)

   + CSONAR_HUB_URL (**IMPORTANT: Deselect "Protected variable"**)


​	The values for these variables will have been given to you by your workshop leader or by CodeSecure Support.

4. **Click** 'Plan => Issues => New Issue'

5. As 'Title' set 'Add CodeSonar', **Click** 'Create Issue', 

6. **Click** 'Create merge request', **Uncheck** 'Mark as draft',

7. Near the bottom of the page, **Click** 'Create Merge Request'

8. In the upper right, **Locate** and **Click** 'Code' (button) and **Select** ‘Open in Web IDE’

   **Note**: A new browser tab opens with VS Code editing a copy of your project.

9. In the file navigation of the VS Code Web IDE, **Navigate to and Click** '.gitlab-ci.yml'

10. **Locate** ‘include:’


11. Add an additional component to the include statement, pointing to the CodeSonar CI component.


   ```
     - component: $CI_SERVER_FQDN/codesonar/components/codesonar-ci/codesonar@1.0.8
       inputs:
         CSONAR_ROOT_TREE: "OSS-Projects/$GITLAB_USER_LOGIN-blinky"
         CSONAR_BUILD_COMMAND: "make -j $(nproc)"
         BUILD_WORK_DIR: "build/pico_w/wifi/blink"  
         CSONAR_PRESET: "-preset misrac2023"
   ```
Make sure to fix the indentation if required. **Note** that we left the GitLab SAST module in the pipeline as well, we can run multiple SAST tools.

  These values are correct for this workshop, for your own projects you would change them appropriately.

  + `CSONAR_ROOT_TREE` is the location in the CodeSonar hub where the project will be
      created, this tree needs to be created in the CodeSonar Hub. If you requested username and password from CodeSecure Support, then this will already be created for you.
  + `CSONAR_BUILD_COMMAND` is the last statement from the build, we will split the build steps in 2 pieces, one to prep the build environment and one to perform the build. In typical CodeSonar fashion this `CSONAR_BUILD_COMMAND` will be wrapped by the codesonar-ci component with the appropriate CodeSonar invocation automatically.
  + `BUILD_WORK_DIR` is the location where the CodeSonar build is invoked, this lines up with this workshop
  + `CSONAR_PRESET` selects which preset to use, for this workshop we will use MISRA C 2023, the latest MISRA C release available.

   The resulting `include:` block should look somethin like this:
   **IMPORTANT**: Be sure the rpi-pico-sdk and it's container are both set to at least version: 0.1.33

   ```
include:
  - component: $CI_SERVER_FQDN/guided-explorations/ci-components/ultimate-auto-semversioning/ultimate-auto-semversioning@1.2.5
  - component: $CI_SERVER_FQDN/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk/rpi-pico-sdk@0.1.32
    inputs:
      PICOSDK_COMP_CONTAINER_TAG: 0.1.32
  - template: Jobs/SAST.gitlab-ci.yml
  - component: $CI_SERVER_FQDN/codesonar/components/codesonar-ci/codesonar@1.0.8
    inputs:
      CSONAR_ROOT_TREE: "OSS-Projects/$GITLAB_USER_LOGIN-blinky"
      CSONAR_BUILD_COMMAND: "make -j $(nproc)"
      BUILD_WORK_DIR: "build/pico_w/wifi/blink"  
      CSONAR_PRESET: "-preset misrac2023"
   ```
| ![CodeSonar Icon](images/CodeSonar.png) ![Checklist icon](images/admonitions/gitlab-logo-20.png) **[Partnership Game Changer]** GitLab CI/CD Components and Catalog |
| ------------------------------------------------------------ |
| CodeSonar is leveraging GitLab's new game changing feature known as CI/CD Components and CI/CD Catalog. Here it is enabling: |
| <ul> <li>CodeSonar to publish a official component for their CI capabilities with code that CodeSecure publishes <li></li>A dramatically simplified coding experience to integrate the CodeSonar scanner <li> Co-customers to depend directly on the CI code in a version safe way </li><li> The community to easily discover their component through the Catalog at https://gitlab.com/explore/catalog </li><li>The ability of self-managed and dedicated customers to copy the component to their instance. </li></ul> |

We are going to change the `pico_build` phase slightly by removing the last 2 steps and moving them to a `before_script` stage. This will make sure that these steps will be executed before the CodeSonar build step. We also need to add .codesonar-build to the `extends` section. 

12. **IMPORTANT:** Remove the last two steps from the `script` section of the .gitlab-ci.yml file, it should end in `cd pico_w/wifi/blink`
13. Change the name of the `script` section to `before_script`.
14. Change the extends to to an array and add  `.codesonar-build` (notice .rpi-pico-sdk needs to stay referenced)


The resulting `pico_build` phase will look like this (note rename to show CS during build):

```
pico_build_with_cs:
  extends: 
    - .rpi-pico-sdk
    - .codesonar-build
  before_script:
    - |
      mkdir build
      cd build
      cmake .. -DPICO_BOARD=pico_w
      cd pico_w/wifi/blink
  artifacts:
    paths:
      - build/pico_w/wifi/blink/picow_blink.uf2
```
15. A last addition is that CodeSonar needs a configuration file, we will create that in the repository. So in your IDE project, **Click** README.md and then near the top of the file list, **Click** `{the New file icon}` to create a new file called `codesonar.conf`. 

16. The pico-example build environment builds a lot of files related to CMake and general tools that we are not interested in from a security analysis perspective. To exclude these files from the analsys, place the following text in `codesonar.conf`:

   ```
   IGNORED_COMPILATIONS += CMakeFiles
   IGNORED_COMPILATIONS += share/cmake
   IGNORED_COMPILATIONS += tools

   MALLOC_FAILURE_BEHAVIOR=DOESNT_FAIL
   ```
   ![Info circle icon](images/admonitions/info-circle25.png)The first three lines make CodeSonar ignore compilation units that originate in directories with these strings in the path. The last line configures the CodeSonar analyzer to not consider paths where malloc fails and returns NULL, which we will need for the test code we will add. The CodeSonar analyzer is highly configurable, more information is available in the [CodeSonar Documentation](https://partnerdemo.codesonar.com/install/codesonar/doc/html/CodeSonar.html#t=WarningClasses%2FLANG%2FLANG.MEM.NPD.html&rhsearch=Null%20pointer) (login required).

| ![CodeSonar Icon](images/CodeSonar.png) CodeSonar Game Changer: Abstract Execution Results By Monitoring Compiler Build |
| ------------------------------------------------------------ |
| CodeSonar goes well beyond traditional SAST detection known as "Code Smells" into the realm of "Abstract Execution". In order to have this capability it actually watches the software compile - hence the build stage was just modified rather than including a new, distinct SAST scanning job. |

17. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

16. In *Commit message*, **Type** ‘Add CodeSonar Scanning’

15. **Click** the button ‘Commit to ’"2-add-codesonar"‘

16. **Close** the Web IDE tab.

17. **Click** {the Merge Request tab from which the IDE was launched}

18. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/pico-examples` and **Click** ‘Code => Merge requests => Resolve “"Add codesonar" ’ 

19. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

20. Wait for all jobs to complete successfully - refresh the page as needed. (Pipeline Status should be ‘Passed’). Executing the pipelines will take a couple of minutes as there is a lot more compute happen to perform the static analysis work to find bugs in the source code. Luckily CodeSonar scales very well, so scaling up the runners will provide significant speed-up if needed.


26. To the left of *Requires 1 approval from New Critical/High Findings*, **Click** 'Approve' (blue button)
27. **Click** 'Merge'
28. On the left navigation, **Click** 'Build => Pipelines'
29. **Ensure** the all pipelines are completed successfully.

### 5.3.2 Use Security Dashboards To See CodeSonar Vulnerabilities in Default Branch (and Production Environment)

| ![](images/admonitions/gitlab-logo-20.png) **[Game Changer]** Security Dashboards |
| ------------------------------------------------------------ |
| While GitLab provides a very shifted left experience when Merge Requests are properly leveraged - it still has traditional “all vulnerabilities” management via security dashboards. This section shows some of the GitLab Security Dashboards. These dashboards track the vulnerabilities that are in the code on the default branch. |

| ![CodeSonar Icon](images/CodeSonar.png) ![Checklist icon](images/admonitions/gitlab-logo-20.png) **[Partnership Game Changer]** Combined Security Results |
| ------------------------------------------------------------ |
| Since CodeSonar is plugged into the GitLab security workflows, the vulnerabilities reported are the combination of all enabled security tools, CodeSonar included, while still enabling the user to run queries for a particular tool |

GitLab Security Dashboards are roughly equivalent to traditional security tool scanning that happens on the entire code base (but NOT shifted left like the MR Developer Experience). This capability is needed for a wholistic approach to vulnerability management.

1. While in a browser tab on `full/path/to/yourgroup/pico-examples` 

2. **Click** ‘Secure => Security Dashboard’

   **NOTE: Each group level above your current project also has the Security Dashboard for ALL child projects to enable team and organization level management of security.**

   > To learn more see the [Security Dashboard documentation](https://docs.gitlab.com/ee/user/application_security/security_dashboard/).

3. **Click** ‘Secure => Vulnerability report’

4. In *Group by* **Click** {in the field at the end of the current filters} and **Select** 'Tool' and then **Select** 'CodeSonar (CodeSecure)'

   **NOTE: Each group level above your current project also has the Vulnerability report dashboard and it shows vulnerabilities for ALL child projects to enable team and organization level management of vulnerabilities.**

   > These vulnerabilities were accepted into the default branch and production by the Merge Approval process we just completed. This report allows the creation of issues to pay down security debt in future sprints. This is also the baseline of vulnerabilities used to generate the “delta” of “new vulnerabilities added by code changes in an MR”
   >
   > To learn more see the [Vulnerability report documentation](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/).

   > In the search bar, you can type `Tool` and then select `CodeSonar (CodeSecure)` to see just the vulnerabilities that CodeSonar has reported.

### 5.3.3 See Security Policy Merge Approvals Block a New Vulnerability During Development

1. While in a browser tab on `full/path/to/yourgroup/pico-examples`

2. **Click** ‘Plan => Issues => New issue’ (button)

3. On *New Issue*, for *Title (required)*, **Type** ‘New Feature’

4. **Click** ‘Create issue’

5. On *Updates {new issue view}*, **Click** ‘Create merge request’ 

6. On *New merge request*, **Uncheck** ‘Mark as draft’

7. Near the bottom of the page, **Click** ‘Create merge request’

8. On *Resolve “"New Feature" {new merge request view}*

9. Find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

10. On the left file navigation, **Navigate to** ‘pico_w/wifi/blink’

11. **Click** 'picow_blink.c'

12. The code to make the LEDs blink is fairly straightforward and does not have very interesting security vulnerabilities. So let's add a bit of test code that is more interesting that we borrowed from an earlier version of [GNU Chess](https://www.gnu.org/software/chess/):

    Add, right before `int main()` and after the last `#include`-line:

    ```c
     #include <stdlib.h>
     // The following is copied from GNU Chess 5.0.8
     char *return_append_str(char *dest, const char *s) {
     /* Append text s to dest, and return new result. */
           char *new_loc;
           size_t new_len;
           /* This doesn't have buffer overflow vulnerabilities, because
              we always allocate for enough space before appending. */
           if (!dest) {
                    new_loc = (char *) malloc(strlen(s))+1;
                    strcpy(new_loc, s);
                    return new_loc;
           }
           new_len = strlen(dest) + strlen(s) + 1;
           new_loc = (char *) malloc(new_len);
           strcpy(new_loc, dest);
           if (!new_loc) return dest; /* Can't do it, throw away the data */
           strcat(new_loc, s);
           return new_loc;
    }
    ```

14. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

15. In *Commit message*, **Type** ‘New Feature’

16. **Click** the button ‘Commit to ’"{number}-new-feature"‘

17. **Close** the Web IDE tab.

18. **Click** {the Merge Request tab from which the IDE was launched}

19. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/pico-examples` and **Click** ‘Code => Merge requests => Resolve “"New Feature" ’ 

20. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

21. Wait For all jobs to complete successfully - refresh the page as needed. (Pipeline Status should be ‘Passed’).

| ![CodeSonar Icon](images/CodeSonar.png) CodeSonar Scalability |
| ------------------------------------------------------------ |
| CodeSonar goes well beyond traditional SAST detection known as "Code Smells" into the realm of "Abstract Execution". In order to have this capability it actually watches the software compile - hence the build stage was just modified rather than including a new, distinct SAST scanning job. |

### 5.3.4 Examining Security Policy Merge Approval Enforcement

1. **Click** 'Overview' (MR Tab)

   The following screenshot can be used to understand the next steps in examining the MR findings and approval status.

![Merge blocked](images/codesonar-blocked.png)

2. Notice that our security policy is triggered by the presence of 1 or more Critical, High or Medium findings for SAST - this is indicated by the MR line *Requires 1 approval from New Critical Findings*

2. Next to *Security scanning detected…* , **Click** {the section expand down arrow}, you should see smoething like:

![Detailed warning view](images/codesonar-findings.png)

3. Notice the finding is only for the code we just changed. All previous findings that were merged are still available in the Security Dashboard - but the MR View focuses on findings created by the code in the current MR.

The warnings here are a mix of warnings from CodeSonar and warnings from GitLab SAST. GitLab scores warnings more aggressively then CodeSonar. 

4. **Click** on one the Medium warning. This particular problem violates a number of MISRA rules.

5. **Click* on the first line under `Identifiers`

![First level](images/codesonar-first-level.png)

![](images/admonitions/classroom20.png) **For Classrooms**: The instructor will provide the username and password for the CodeSonar login.

6. This will pop up CodeSonar, log in with your credentials, you should see the warning page. The tan background is the warning path, you can see that the true branch is taken for the if statement on line 17. 

   | ![CodeSonar Icon](images/CodeSonar.png) CodeSonar Deep Scanning to Meet Compliance and Standards Requirements |
   | ------------------------------------------------------------ |
   | These type of deeper warnings are what CodeSonar is known for and which enable it to make large contributions to being compliant with the standards, laws and executive orders surrounding security of embedded systems. |



![Warning page](images/codesonar-warning-view.png)

7. It is interesting to see the difference between GitLab SAST and CodeSonar SAST. Both deliver value, but they report at a different level. For example, look at the GitLab SAST warning `Insecure string processing function (strcpy)`. The warning states that strcpy is insecure and can lead to buffer overruns, which is true. In this case it points to the line

    ```c
                        strcpy(new_loc, s);
    ```
    
    ![CodeSonar Icon](images/CodeSonar.png)  CodeSonar understands the code at a deeper level and goes one step further, as is evidenced by the screen shot above. CodeSonar states that *in this particular use of strcpy, in this piece of code* there is a buffer overrun as the malloc on line 18 is not allocating sufficient memory (even though the comment on line 15/16 indicates that sufficient memory is being allocated).
    
    The problem here is the `malloc(strlen(s))+1`, the correct code is `malloc(strlen(s)+1)`, note the subtle difference in placement of the closing bracket.
    
    The CodeSonar warning has even deeper information that would benefit the safety, security and compliance of the code when analyzed by a C Language developer.

### 5.3.5 Removing the Code To See The Security Policy Automatically Become Optional

1. While in a browser tab on `full/path/to/yourgroup/pico-examples`, **Click** ‘Code => Merge requests => Resolve “"New Feature" ’  (or switch to the tab where this is already open)

3. On *Resolve “"New Feature" {new merge request view}*

4. Find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

5. On the left file navigation, **Navigate to** ‘pico_w/wifi/blink’

6. **Click** 'picow_blink.c'

7. Instead of fixing the findings, like you would do in a real-world project, we will remove all the inserted code between `\#include "pico/cyw43_arch.h"` and  `int main()`  (including `#include <stdlib.h>`)
   The result should look like this (just the line before and the line after the removed code are depicted below):

   ```c
   #include "pico/cyw43_arch.h"
   int main() {
   ```

8. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

9. In *Commit message*, **Type** ‘Fix vulnerability’

10. **Click** the button ‘Commit to ’"{number}-new-feature"‘

11. **Close** the Web IDE tab.

12. **Click** {the Merge Request tab from which the IDE was launched}

13. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/pico-examples` and **Click** ‘Code => Merge requests => Resolve “"New Feature" ’ 

14. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

15. Wait For all jobs to complete successfully - refresh the page as needed. (Pipeline Status should be ‘Passed’).

16. **Click** 'Overview' (MR Tab)

    The following screenshot can be used to understand the next steps in examining the MR findings and approval status.

    ![](images/mr_pipeline_pass_codesonar.png)

17. Notice:

    1. MR Approval 'New High Findings' is now optional
    2. The MR is ready to merge.
    3. The Security Bot shows violations are resolved (also emailed as build status)


| ![](images/admonitions/gitlab-logo-20.png) **[Game Changer]** Voluntary Developer Initiated Remediation Management |
| ------------------------------------------------------------ |
| When developers have:<ul> <li> Timely vulnerability information <li> That relates to only the code they've been changing <li> With clear personal responsibility<li> and helps like just in time training and collaboration<li> process automation which means no unnecessary human-led steps (policy enforcement)</ul>They are very likely to resolve software defects because they want to create great, defect-free code. |

18. **Click** 'Merge'
18. **Ensure** the Merge Request Pipeline completes successfully.


### 5.3.6 Changing CodeSonar Project Structure

![](images/admonitions/classroom20.png) **For Classrooms**: Just read through this information to learn more about CodeSonar customization.

Developers can perform all their work in the Web IDE, or in IDEs such as VS Code and drive all their daily activities in-context and in their normal development workflow. CodeSonar works in the background and can provide another, optional, view of the analyzed code. The CodeSonar user interface is available at the hub address, for this excercise the hub is at [https://partnerdemo.codesonar.com](https://partnerdemo.codesonar.com) and you can access it with the provided credentials.

There will be a folder with the name `<your-gitlab-id>-blinky` and projects underneath that. Every branch has it's own project (based on the branch name) and every push to the branch gets its own analysis. Best practice is to make a folder for all the merge request (aka feature) branches.

1. In CodeSonar, **click** `Create Project Tree`
2. Name the tree `feature`
3. In your GitLab UI browse to `full/path/to/yourgroup/pico-examples` and **Click** `Settings => Repository`
4. **Click** `Expand` next to `Branch Defaults`
5. Set `Branch name template` to `feature/%{id}-%{title}` and **Click** Save changes

Now, next time that you make a new merge request and push changes, the analysis will be stored in the `feature` project tree.

### 5.3.7 Additional CodeSonar Views

The same warnings that you can see in GitLab are also visible in the CodeSonar UI.

1. In the CodeSonar, open your blinky project, `master` project, this corresponds to the `master` branch in your GitLab repo. The project is likely in `Home -> OSS-Projects -> {Your GitLab id}-blinky -> master`
2. **Click** 'Reports'
3. For *Report Template*, **Select** 'MISRA C 2023'
4. For *Report Format*, **Click** 'view html report', this shows the MISRA violations per MISRA rule.
5. Go back to the `master` project, the breadcrumbs at the top of the page are convenient for this, when you click on master in the breadcrumbs you see the list of analysis that were done on the `master` branch. Typically you click the last one.

![breadcrumbs](images/breadcrumbs.png)

3. **Click** {the browser back button} to go back to the latest analysis on the `master` project 

4. **Click** 'Charts and Tables'

5. Next to *warnings by class*, **Click** 'chart'

   > This is a good view to see how many warnings there are for each class. 

6. **Click** on {one of the colored bars} to see just the warnings for that class.

7. **Click** on one of the warnings, this gives more information about a particular warning, including the warning path in the tan background

8. **Hover** over a variable or function call and you will see the info window pop up in the lower right, this is a useful tool to understand the code

![Info window](images/info-window.png)

7. Just above the code, middle of the page, **Click** 'warning details' close to the top of the screen, this lists the MISRA rules that this warning is related to.
8. In the *Warning details* grey box, under *Explore Callers*, **Click** 'graphical (lite)', this shows a call-tree (when relevant) as to how this warning can happen in the larger program. Here is an example of a complex mapping:

![Call tree](images/call-tree.png)

9. CodeSonar has a lot more capabilities, feel free to explore the documentation under the `Question Mark` in the upper right hand side.

| ![CodeSonar Icon](images/CodeSonar.png) CodeSonar Infrastructure Options |
| ------------------------------------------------------------ |
| GitLab has elaborate deployment options, including self-hosted deployments. CodeSonar fully supports that model. In this workshop, we have used SaaS shared runners on the GitLab infrastructure connected to a CodeSonar Hybrid-SaaS instance, hosted by CodeSecure. |
