# Embedded DevOps Workshop - A Self-Paced POC for Refactoring to GitLab CI and Modern Security and Compliance

![](images/embeddeddevopsbenefits.png)

# GitLab Developer Experiences and Game Changers

The following are some of the valuable things that this working code and the tutorials demonstrate.

![](images/admonitions/gitlab-logo-20.png) = Unique GitLab Value.
![](images/admonitions/gitlab-logo-20.png) **[Game Changer]** = GitLab Value That is Game Changing.

| **Valuable Developer Experiences and Solutions Demonstrated** | [Tutorial 1](TUTORIAL1.md) | [Tutorial 2](TUTORIAL2.md) | [Tutorial 3](TUTORIAL3.md) | [Tutorial 3.1](TUTORIAL3.1.md) | [Tutorial 4](TUTORIAL4.md) | [Tutorial 5.2](TUTORIAL5.2.md) | [Tutorial 5.3](TUTORIAL5.3.md) | [Tutorial 6](TUTORIAL6.md) |
| :----------------------------------------------------------- | :------------------------: | :------------------------: | :------------------------: | :----------------------------: | :------------------------: | :----------------------------: | ------------------------------ | -------------------------- |
| **Time Required** (Hands On Mins / Automation Wait Mins)     |                            |                            |                            |                                |                            |                                |                                |                            |
| **Baseline Pre-DevOps Embedded Development**<br />Emulating how embedded development happens on real HW to show a baseline that will then be transitioned to a DevOps SDLC in subsequent Labs. |     :white_check_mark:     |                            |                            |                                |                            |                                |                                |                            |
| ![](images/admonitions/gitlab-logo-20.png) **Separating and Virtualizing Build Tools Into Containers** |                            |     :white_check_mark:     |                            |                                |                            |                                |                                |                            |
| ![](images/admonitions/gitlab-logo-20.png) **Separating and Virtualizing Build Scripts Into CI Code** |                            |     :white_check_mark:     |                            |                                |                            |                                |                                |                            |
| ![](images/admonitions/gitlab-logo-20.png) **Firmware Packaging, Storage and Release Outside of Git Storage** |                            |                            |     :white_check_mark:     |                                |                            |                                |                                |                            |
| ![](images/admonitions/gitlab-logo-20.png) **Certficateless Signing and SLSA 2 Attestation for Firmware** |                            |                            |                            |       :white_check_mark:       |                            |                                |                                |                            |
| ![](images/admonitions/gitlab-logo-20.png) **Creating a GitLab Runner Gateway for Embedded Boards and Flashing Firmware in CI**<br />Shows how GitLab CI is capable of Hardware in the CI Loop. |                            |                            |                            |                                |     :white_check_mark:     |                                |                                |                            |
| ![](images/admonitions/gitlab-logo-20.png) **[Game Changer] Security Scanning in Merge Requests with Security Policy Merge Approvals**<br />**Game Changer Because**: Security vulnerabilities that were just introduced, are immediately evident to the developer who accidentally introduced them and they are blocked from merging forward to shared branches. |                            |                            |                            |                                |                            |       :white_check_mark:       |                                |                            |
| ![CodeSonar Icon](images/CodeSonar.png) ![Checklist icon](images/admonitions/gitlab-logo-20.png) **[Partnership Game Changer]** CodeSonar deep C Language scanning results enabled in Security Dashboards, MRs and Security policies. [Supports MISRA, AUTOSAR, ISO 26262, IEC 61508, and EN 50128](https://codesecure.com/our-products/codesonar/#fndry-block-64237cde9ec59). [Certified for standards](https://www.exida.com/SAEL-Safety/grammatech-codesonar-static-analysis-tool). |                            |                            |                            |                                |                            |       :white_check_mark:       | :white_check_mark:             |                            |
| ![](images/admonitions/gitlab-logo-20.png) **Manage a Pico W Device Cloud in GitLab and Use Docker Executor for HCIL Jobs**<br />Shows how GitLab CI is capable of Sharing Hardware in the CI Loop as a Cloud. Demonstrate Docker executor managing embedded boards for cleaner builds |                            |                            |                            |                                |                            |                                |                                | :white_check_mark:         |

## Requirements

Hardware, software and user account requirements are covered in the [Prerequisites Document](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)

## Get Started With This Workshop

This workshop can be completed completely self-paced in about 4-5 hours.

1. [Buy Your Hardware](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)
2. [Prep Your Hardware](TUTORIAL1-PREP.md)
3. [Tutorial 1: Manual Build: Blinking Light On Raspberry Pi W](TUTORIAL1.md)
4. [Tutorial 2: Automating Build to GitLab CI](TUTORIAL2.md)
5. [Tutorial 3.1: Firmware Packaging, Storage and Release](TUTORIAL3.1.md)
6. [Tutorial 3.2: Adding Sigstore Certificateless Signing and SLSA v1.0 Attestation For Firmware Binaries](TUTORIAL3.2.md)
7. [Tutorial 4: Setup GitLab Runner Gateway to Flash Firmware](TUTORIAL4.md)
8. [Tutorial 5.1: Security Dashboards and Security Policy Merge Approval Rule Configuration](TUTORIAL5.1.md)
8. [Tutorial 5.2: Security Policy Merge Approvals In Action with GitLab SAST](TUTORIAL5.2.md)
9. [Tutorial 5.3: Security Policy Merge Approvals with CodeSonar SAST Component](TUTORIAL5.3.md)
10. [Tutorial 6: Create a Pico W Device Cloud in GitLab and for Hardware in the CI Loop Testing](TUTORIAL6.md)