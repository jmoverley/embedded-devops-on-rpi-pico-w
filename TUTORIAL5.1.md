# Tutorial 5.1: Security Dashboards and Security Policy Merge Approval Rule Configuration

## Known Working Version Details

Tested Date: **2024-06-12**

Testing Version (GitLab and Runner): **Enterprise Edition 17.0.0 SaaS**

To report problems, [check the issues here](https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-on-rpi-pico-w) to see if it is already known before creating a new issue in that project.

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
  **IMPORTANT**: Generally you are asked to close Web IDE tabs immediately after commiting because using old ones causes Merge Conflicts if the code has been updated in another Web IDE tab.

## Prerequisties

There is nothing here that depends on GitLab.com, so it should work fine on other Gitlab instances that are licensed for Ultimate.

**IMPORTANT:** 

- General Requirements for self-paced execution of this tutorial are covered in [SELFPACED-INSTRUCTOR-DEMOER-Requirements.md](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md)
- This tutorial requires that Tutorials 1-4 must also be completed and the resultant project available to start this tutorial.

[Authored as Open Educational Resources (OER) and with Hyperscaling Enablement Content Architecture (HECA)](OPEN-EDUCATION-RESOURCES-HECA.md)

| ![Checklist icon](images/admonitions/list-ul25.png)  Learning Objectives and Concepts |
| ------------------------------------------------------------ |
| - GitLab Security Policy Merge Approval rules are stored as code<br />- Security Policies are created in a seperate repository and can be applied to many projects in a hierarchy<br />![](images/admonitions/lab-answers25.png) **Lab Answers:** [labsections2-5.2gitlab-ci.yml](../../solutions/labsections2-5.2gitlab-ci.yml) |

## Exercises

### 5.1.1 Adding Security Scanning to Merge Request

1. Recenter your location by browsing to `full/path/to/yourgroup/pico-examples` 

2. **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

3. In the upper right, **Locate** and **Click** 'Code' (button) and **Select** ‘Open in Web IDE’

   **Note**: A new browser tab opens with VS Code editing a copy of your project.

4. In the file navigation of the VS Code Web IDE, **Navigate to and Click** '.gitlab-ci.yml'

5. **Locate** ‘include:’

6. At the bottom add these two references (with proper indenting):

   ```
     - template: Jobs/SAST.gitlab-ci.yml
   ```

   The resulting `include:` block should look like this:

   ```
   include:
     - component: $CI_SERVER_FQDN/guided-explorations/ci-components/ultimate-auto-semversioning/ultimate-auto-semversioning@1.2.5
     - component: $CI_SERVER_FQDN/guided-explorations/embedded/ci-components/arm-gnu-embdd-rpi-pico-sdk/rpi-pico-sdk@0.1.30
       inputs:
         PICOSDK_COMP_CONTAINER_TAG: 0.1.30
     - template: Jobs/SAST.gitlab-ci.yml
   ```

7. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines)

8. In *Commit message*, **Type** ‘Add Security Scanning’

9. **Click** the button ‘Commit to ’"1-updates"‘

10. **Close** the Web IDE tab.

11. **Click** {the Merge Request tab from which the IDE was launched}

12. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/pico-examples` and **Click** ‘Code => Merge requests => Resolve “"Updates" ’ 

13. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

14. Wait For all jobs to complete successfully - refresh the page as needed. (Pipeline Status should be ‘Passed’).

15. Near the top, under the MR title, **Click** 'Overview' (tab)

16. There should be an expandable section for “Approval is optional” - **Click** {the arrow on the right}

17. There should be an expandable section for “Security scanning detected…” - if it is not visible or not yet able to be expanded, keep refreshing your browser until the sections are completely populated and expandable.

    You should be seeing something like this:
    ![MR view](images/mrviewwithfindings.png)

    | ![Checklist icon](images/admonitions/only-learning-flask25.png) Exercise Only: MR View of Vulnerabilities of Existing Vulnerabilities |
    | ------------------------------------------------------------ |
    | For the exercise we cheated to make it look like you just created all the code in the imported project. So we are seeing all the vulnerabilities as though you just coded these in your MR. In the following exercises you will see this work with only actual changes you make. |

18. **Click** 'Merge'

19. On the left navigation, **Click** 'Build => Pipelines'

20. **Ensure** the all pipelines are completed successfully.

### 5.1.2 Use Security Dashboards To See Vulnerabilities in Default Branch (and Production Environment)

| ![](images/admonitions/gitlab-logo-20.png) **[Game Changer]** Security Dashboard |
| ------------------------------------------------------------ |
| While GitLab provides a very shifted left experience when Merge Requests are properly leveraged - it still has traditional “all vulnerabilities” management via security dashboards. This section shows some of the GitLab Security Dashboards. These dashboards track the vulnerabilities that are in the code on the default branch. |

GitLab Security Dashboards are roughly equivalent to traditional security tool scanning that happens on the entire code base (but NOT shifted left like the MR Developer Experience). This capability is needed for a wholistic approach to vulnerability management.

1. While in a browser tab on `full/path/to/yourgroup/pico-examples` 

2. **Click** ‘Secure => Vulnerability report’

   **NOTE: Each group level above your current project also has the Vulnerability report dashboard and it shows vulnerabilities for ALL child projects to enable team and organization level management of vulnerabilities.**

   > These vulnerabilities were accepted into the default branch and production by the Merge Approval process we just completed. This report allows the creation of issues to pay down security debt in future sprints. Dependency vulnerabilities are listed here. This is also the baseline of vulnerabilities used to generate the “delta” of “new vulnerabilities added by code changes in an MR”
   >
   > To learn more see the [Vulnerability report documentation](https://docs.gitlab.com/ee/user/application_security/vulnerability_report/).

3. **Click** ‘Secure => Dependency list’

   **NOTE: Each group level above your current project also has the Dependency list dashboard and it shows Dependencies for ALL child projects to enable team and organization level management of dependencies.**

   > This is probably empty for your project because Embedded development with C Lang does not use a dependency framework. This would be different if your project was using Rust. This is a complete list of the dependencies in your software - which is part of the Software Bill of Materials (SBOM) and it also notes which ones are vulnerable.
   >
   > To learn more see the [Dependency list documentation](https://docs.gitlab.com/ee/user/application_security/dependency_list/).x

### 5.1.3 Adding Security Policy Merge Approval Rules

| ![](images/admonitions/gitlab-logo-20.png) **[Game Changer]** Automatic Security Policies That Prevent New Vulnerabilities During Development |
| ------------------------------------------------------------ |
| The concept of "Security Policy" means this capability is completely automated. If the developer's **new** code contains a **new** vulnerability, it will be blocked. Security is not notified because there is no risk to the organization if the code can't merge to the next shared branch. If the developer resolves the vulnerability, the Merge Approval blocking based on security policy automatically becomes optional. No security team involvement, but full enforcement of security rules. |

1. Recenter your location by browsing to `full/path/to/yourgroup/pico-examples` 

2. On the left navigation, **Click** ‘Secure => Security configuration => Vulnerability Management’ (last click is a tab under the page heading)

3. On *Security configuration*, under *Security training*, **Click** each of the {slider buttons next to ‘Kontra’, ‘Secure Code Warrior’, ‘SecureFlag’}

   | ![](images/admonitions/gitlab-logo-20.png) **[Game Changer]** Just In Time Training On Specific Vulnerabilities |
   | ------------------------------------------------------------ |
   | Each of these GitLab partners can provide "Per-Vulnerability" micro-training - so developers only have to learn about vulnerabilities they actually create in their code, rather than attempting to pre-learn about the thousands of possible vulnerabilities. |

4. On the left navigation, **Click** 'Secure => Policies'

5. On the *Policies* page, in the upper right, **Click** 'New policy'

6. On the *New policy* page, in the *Merge request approval policy* panel, **Click** 'Select Policy'

7. On the next *New policy* page, in *Name* **Type** 'New Critical/High Findings'

8. Under *Description*, **Type** 'If you introduce new Critical or High vulnerabilities, security must approve to merge them. If you remove the new vulnerabilities, this approval becomes optional instead of required.'

9. Under *Rules*, make the following selections for the indicated field (leave all others at default):

   1. **When** = 'Security Scan'
   2. **All scanners** = Ensure only SAST is selected  (**IMPORTANT**).
   3. **all protected branches, No exceptions, Any** = leave at defaults, but you may want to click the drop down to examine the values to discover what else can be done.
   4. **Severity is** = Ensure the only selected items are 'Critical' and 'High'
   5. **Status is** = leave at defaults, but you may want to click the drop down to examine the values to discover what else can be done.
   6. Under *Actions*, **Choose approver type** = 'Roles'
   7. **Choose specific role** (just appeared) = 'Owner' and 'Maintainer'

10. Under *Override project approval settings*, **Examine** {all the settings} to discover what else can be controlled for the target branches.

    Note: This section applies all the time to the branches in view - not just when the rules are triggered.

11. In Override project approval settings, **Deselect** 'Prevent approval by merge request's author'

12. Also **Deselect** 'Prevent approval by commit author'

    | ![](images/admonitions/only-learning-flask25.png) Only For Learning |
    | ------------------------------------------------------------ |
    | Disabling 'Prevent approval by author' is only being done so that it is easy for the future exercises to proceed without needing a second user ID to approve a security exception. Whether this setting is disabled in production will need to be a thoughtful decision. |

13. At *the bottom of the page*, **Click** 'Configure with a merge request'

    Note: This is creating a new project - not editing the project you started in.

14. On the new MR titled *Update scan policies*, **Select** 'Delete source branch' 

15. **Click** 'Merge' (blue button).

16. At the top of the screen, in the breadcrumb trail, note that you are in a new project called "Pico Examples Security policy project"

17. On the left navigation, **Click** 'Code => Repository'

18. In the file list mid-page, **Click** '.gitlab/security-policies'

19. In the file list mid-page, **Click** 'policy.yml'

    | ![](images/admonitions/gitlab-logo-20.png) **[Game Changer]** Everything as Code |
    | ------------------------------------------------------------ |
    | The GUI that you manipulated simply created a merge request to merge this YAML file. Since this created "Security Policy as Code", it is include in templates, replicate, generate or change the policy YAML using code. Anything stored in Git as code also has full change control (via MRs), version control and version history. |

20. Navigate back to `full/path/to/yourgroup/pico-examples` 

21. On the left navigation, **Click** 'Secure => Policies'

22. You should see your new rule 'New High Findings'

    | ![](images/admonitions/gitlab-logo-20.png) **[Game Changer]** Security Policy Merge Approvals |
    | ------------------------------------------------------------ |
    | While authoring a new rule created the policy as yaml in this new project, the project could now be applied to hundreds of projects as the rule set without doing it for each project.<br /><br />By being housed in an external repository, permissions to the policy files can be controlled by a security team. Changes to the policies can use standard code collaboration and change management processes. |

23. On the {left navigation}, ***Click** ‘Settings => Merge requests’ (**NOT** “Code => Merge Requests”)

24. Quite a ways down the page, under *Approvel settings*, if not already done, **Deselect** ‘Prevent approval by author’ 

25. If it is selected, **Deselect** ‘Prevent approvals by users who add commits’

    | ![](images/admonitions/only-learning-flask25.png)  Only For Learning |
    | ------------------------------------------------------------ |
    | Disabling 'Prevent approval by author' is only being done so that it is easy for the future exercises to proceed without needing a second user ID to approve a security exception. Whether this setting is disabled in production will need to be a thoughtful decision. |

26. Under the section *Approval settings*, **Click** ‘Save changes’ (button) 

    ![](images/admonitions/danger-skull25.png)  **IMPORTANT:** Do not use any other ‘Save changes’ button on the page but the next immediate one below the setting just changed.

| ![Checklist icon](images/admonitions/success-check-circle25.png)  Accomplished Outcomes |
| ------------------------------------------------------------ |
| - GitLab Security Policy Merge Approval rules are stored as code.<br />- First, baseline scan for future MRs to only show new vulnerabilities introduced by code in an MR. |

