# 	Tutorial 3.2: Adding Sigstore Certificateless Signing and SLSA v1.0 Attestation For Firmware Binaries

## Known Working Version Details

Tested Date: **2024-06-27**

Testing Version (GitLab and Runner): **Enterprise Edition 17.0.0 SaaS**

Tested Hardware: Raspberry Pi 3A+ 512MB (as "Big Pi"), Raspberry Pi 4b 4GB (as "Big Pi"), Raspberry Pi Pico

To report problems, [check the issues here](https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-on-rpi-pico-w) to see if it is already known before creating a new issue in that project.

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs. 
  **IMPORTANT**: Generally you are asked to close Web IDE tabs immediately after commiting because using old ones causes Merge Conflicts if the code has been updated in another Web IDE tab.

## Prerequisites

This tutorial requires that Tutorial 1 through 3 have been completed.

No subsequent tutorials depend on this one.

| ![Small Tanuki](images/admonitions/gitlab-logo-20.png) GitLab.com Required for Sigstore |
| ------------------------------------------------------------ |
| GitLab.com is preconfigured for interfacing with Sigstore. Configuring Sigstore for self-managed is possible, but complex and not enabled by default. |

| ![Checklist icon](images/admonitions/list-ul25.png)  Learning Objectives and Concepts |
| ------------------------------------------------------------ |
| - Learn some recent software packaging technologies that help with attestation and audit of firmware binaries and how easily they can be implemented by GitLab<br />- Perform Sigstore signing and signature verification of firmware binary<br />- Enable SLSA Attestation for Generated Artifacts<br />![labsetup](images/slsaattestation.png) |

| ![](images/admonitions/lightbulb25.png) Electronic Copy of Workshop Exercises |
| ------------------------------------------------------------ |
| Since there is a lot of code copying from these exercises, you may wish to open them in a dedicated browser window at:  https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-workshop-refactoring-to-ci |

## Exercises

| ![Small Tanuki](images/admonitions/gitlab-logo-20.png) GitLab Game Changer: Integrated Compliance for SLSA v1.0, L1 |
| ------------------------------------------------------------ |
| When the Gitlab DevSecOps platform integrates new compliance capabilities it is made as seamless and easy to configure as possible. In this case the addition of a single variable adds SLSA v1.0 to our pipeline artifacts. |

### 3.2.1 Add Sigstore Certificateless Signing

**Instructors:** This lab may be skipped if time is tight.

| ![](images/admonitions/security-compliance25.png) Security and Compliance: Provenance and Chain of Custody Using Sigstore Certificateless Signing |
| ------------------------------------------------------------ |
| Signing code, binaries and packages helps your code and development processes to be both secure and compliant.<br />Sigstore takes a unique approach of using temporary certificates, signing your artifact, recording the signing in a block-chain like internet store and then discarding the keys. This gives the following benefits:<br />- there is no key management required and no risk of leaked keys.<br />- Signing information is stored by a trusted third party and available publicly. |

1. This block will now be updated to add Sigstore signing

   ```yaml
   create_generic_package:
     stage: deploy
     image: bash
     before_script:
      - apk add curl
     script:
       - |
         if [[ "${CI_SCRIPT_TRACE}" == "true" ]] || [[ -n "${CI_DEBUG_TRACE}" ]]; then
           echo "Debugging enabled"
           set -xv
         fi
         cd build/pico_w/wifi/blink
         echo "picow_blink.uf2" >> includefiles.lst
         tar -czvf ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz -T includefiles.lst
         echo "Pushing package to registry."  
         curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${GitVersion_LegacySemVer}/${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz
   ```

2. Right before `before_script:` add these lines (paying attention to indenting the 2 spaces to be part of the job

   ```yaml
     variables:
       COSIGN_YES: "true"
     id_tokens:
       SIGSTORE_ID_TOKEN:
         aud: sigstore
   ```

3. Update `before_script:` to add the signing utility `cosign` to the package list so it looks like this:

   ```yaml
     before_script:
      - apk add curl cosign
   ```

4. In the `script:` section, immediately after `cd build/pico_w/wifi/blink` add these lines:

   ```yaml
         echo "Generating firmware signature"
         cosign sign-blob picow_blink.uf2 --bundle picow_blink.${GitVersion_LegacySemVer}.cosign.bundle
         echo "Verifying signature"
         cosign verify-blob picow_blink.uf2 --bundle picow_blink.${GitVersion_LegacySemVer}.cosign.bundle --certificate-identity "${CI_PROJECT_URL}//.gitlab-ci.yml@refs/heads/${CI_COMMIT_REF_NAME}" --certificate-oidc-issuer "https://gitlab.com"
         echo "Bundling signature with binary"
         echo "picow_blink.${GitVersion_LegacySemVer}.cosign.bundle" >> includefiles.lst      
   ```

5. Immediately after `tar -czvf ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz -T includefiles.lst`, add:

   ```yaml
         echo "Signing firmware archive"
         cosign sign-blob ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz --bundle ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz.cosign.bundle
         echo "Verifying signature"
         cosign verify-blob ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz --bundle ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz.cosign.bundle --certificate-identity "${CI_PROJECT_URL}//.gitlab-ci.yml@refs/heads/${CI_COMMIT_REF_NAME}" --certificate-oidc-issuer "https://gitlab.com"
   
   ```

6. The result should look like this:

   ````yaml
   create_generic_package:
     stage: deploy
     image: bash
     variables:
       COSIGN_YES: "true"
     id_tokens:
       SIGSTORE_ID_TOKEN:
         aud: sigstore
     before_script:
      - apk add curl cosign
     script:
       - |
         if [[ "${CI_SCRIPT_TRACE}" == "true" ]] || [[ -n "${CI_DEBUG_TRACE}" ]]; then
           echo "Debugging enabled"
           set -xv
         fi
         cd build/pico_w/wifi/blink
         echo "Signing firmware"
         cosign sign-blob picow_blink.uf2 --bundle picow_blink.${GitVersion_LegacySemVer}.cosign.bundle
         echo "Verifying signature"
         cosign verify-blob picow_blink.uf2 --bundle picow_blink.${GitVersion_LegacySemVer}.cosign.bundle --certificate-identity "${CI_PROJECT_URL}//.gitlab-ci.yml@refs/heads/${CI_COMMIT_REF_NAME}" --certificate-oidc-issuer "https://gitlab.com"
         echo "picow_blink.${GitVersion_LegacySemVer}.cosign.bundle" >> includefiles.lst      
         echo "picow_blink.uf2" >> includefiles.lst
         tar -czvf ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz -T includefiles.lst
         echo "Signing firmware archive"
         cosign sign-blob ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz --bundle ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz.cosign.bundle
         echo "Verifying signature"
         cosign verify-blob ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz --bundle ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz.cosign.bundle --certificate-identity "${CI_PROJECT_URL}//.gitlab-ci.yml@refs/heads/${CI_COMMIT_REF_NAME}" --certificate-oidc-issuer "https://gitlab.com"
         echo "Pushing package to registry."  
         curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${GitVersion_LegacySemVer}/${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz
   ````

7. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines with a colored number badge)

8. In *Commit message*, **Type** 'Add a release'

9. **Click** ‘Commit to '1-updates'

   > When working in GitLab’s integrated editing environments, creating a commit is also equivalent to doing a Git push operation, which will also trigger CI checks when CI is enabled.

10. **Close** the Web IDE tab.

11. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab) and wait for the pipeline to successfully complete before continuing.

12. On the left navigation **Click** 'Deploy => Releases'

13. For the release at the top of the page, under *Other*, **Right Click** {the firmware package url}. and **Select** 'Copy Link Address'

14. SSH into your Big Pi.

15. Change to your home directory with 

    ```
    cd ~
    ```

16. **Type** 'curl -O ' and paste your url for something that looks like this (url will be different):

    ```
    curl -O https://gitlab.com/api/v4/projects/58387288/packages/generic/pico-examples/0.1.147-1-updates21/pico-examples.0.1.147-1-updates21.tar.gz
    ```

17. **Press** {enter} 

18. Expand the archive with this command - be sure to update the filename to match exactly what was just downloaded as you may have more than one archive in the directory now (file name will be different): 

    ```
    tar -xvf pico-examples.0.1.147-1-updates21.tar.gz
    ```

19. The output should show the file picow_blink.uf2 was extracted as well as a file ending in .cosign.bundle

| ![Info circle icon](images/admonitions/info-circle25.png)  More Sigstore Information |
| ------------------------------------------------------------ |
| For more advanced use cases for Sigstore, visit the documentation page: [Signing With Blobs](https://docs.sigstore.dev/signing/signing_with_blobs/) |

| ![Small Tanuki](images/admonitions/gitlab-logo-20.png) GitLab Game Changer: Just a Few Lines for Modern, Certificateless Codesigning |
| ------------------------------------------------------------ |
| The integration of Sigstore means that it only takes a few lines to implement the very modern practice of code signing without the headache of certificate management. |

### 3.2.2 Add SLSA v1.0 Artifact Attestation

The CI Job needs to be updated so that we can capture the artifacts of the last build and create an archive and release that contains the firmware images.We will now use the code editor on the Merge Request so we end up editing our specific branch. If you accidentally click it back in the repository it will load the default branch and none of your changes will be present.

1. Recenter your location by browsing to `full/path/to/yourgroup/pico-examples` 

2. **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

3. In the upper right, **Locate** and **Click** 'Code' (button) and **Select** ‘Open in Web IDE’

   **Note**: A new browser tab opens with VS Code editing a copy of your project.

4. In the file navigation of the VS Code Web IDE, **Navigate to and Click** '.gitlab-c.yml'

5. Immediately above sages: `[.pre, build, test, deploy, hil_test, release, .post]` add these lines (this is not part of a job)

   ```yaml
   variables:
     RUNNER_GENERATE_ARTIFACTS_METADATA: "true"
   ```

6. At the bottom of the job titled `create_generic_package:` add these lines:

   ```
     artifacts:
       name: "FirmwareBinaries"
       paths:
         - build/pico_w/wifi/blink/picow_blink.uf2
         - build/pico_w/wifi/blink/picow_blink.${GitVersion_LegacySemVer}.cosign.bundle
         - build/pico_w/wifi/blink/${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz
         - build/pico_w/wifi/blink/${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz.cosign.bundle
   ```

   > `artifacts:` should be at the same indent level as the `script:` keyword before the script.

7. After these changes, the `create_generic_package:` job will look something like this:

   ```yaml
   create_generic_package:
     stage: deploy
     image: bash
     variables:
       COSIGN_YES: "true"
     id_tokens:
       SIGSTORE_ID_TOKEN:
         aud: sigstore
     before_script:
       - apk add curl cosign
     script:
       - |
         if [[ "${CI_SCRIPT_TRACE}" == "true" ]] || [[ -n "${CI_DEBUG_TRACE}" ]]; then
           echo "Debugging enabled"
           set -xv
         fi
         cd build/pico_w/wifi/blink
         echo "Signing firmware"
         cosign sign-blob picow_blink.uf2 --bundle picow_blink.${GitVersion_LegacySemVer}.cosign.bundle
         echo "Verifying signature"
         cosign verify-blob picow_blink.uf2 --bundle picow_blink.${GitVersion_LegacySemVer}.cosign.bundle --certificate-identity "${CI_PROJECT_URL}//.gitlab-ci.yml@refs/heads/${CI_COMMIT_REF_NAME}" --certificate-oidc-issuer "https://gitlab.com"
         echo "picow_blink.${GitVersion_LegacySemVer}.cosign.bundle" >> includefiles.lst      
         echo "picow_blink.uf2" >> includefiles.lst
         tar -czvf ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz -T includefiles.lst
         echo "Signing firmware archive"
         cosign sign-blob ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz --bundle ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz.cosign.bundle
         echo "Verifying signature"
         cosign verify-blob ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz --bundle ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz.cosign.bundle --certificate-identity "${CI_PROJECT_URL}//.gitlab-ci.yml@refs/heads/${CI_COMMIT_REF_NAME}" --certificate-oidc-issuer "https://gitlab.com"
         echo "Pushing package to registry."  
         curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${GitVersion_LegacySemVer}/${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz
     artifacts:
       name: "FirmwareBinaries"
       paths:
         - build/pico_w/wifi/blink/picow_blink.uf2
         - build/pico_w/wifi/blink/picow_blink.${GitVersion_LegacySemVer}.cosign.bundle
         - build/pico_w/wifi/blink/${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz
         - build/pico_w/wifi/blink/${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz.cosign.bundle
   ```

8. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines with a colored number badge)

9. In *Commit message*, **Type** 'Add SLSA v1'

10. **Click** ‘Commit to '1-updates'

   > When working in GitLab’s integrated editing environments, creating a commit is also equivalent to doing a Git push operation, which will also trigger CI checks when CI is enabled.

11. **Close** the Web IDE tab.
12. **Click** {the Merge Request tab from which the IDE was launched}
13. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/pico-examples` and **Click** ‘Code => Merge requests => Resolve “Updates” ’ 
14. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)
15. **Click** {the pipeline bubble} to expand the details.
16. When the job `pico_build` completes, **Click** 'pico_build'
17. On the right, under *Job Artifacts*, **Click** 'Browse'
18. **Click** 'FirmwareBinaries-metadata.json' (may also be called artifacts-metadata.json)
19. If you see *You are being redirected away from GitLab*, **Click** {the file url}
20. You are now browsing the SLSA 1.0 manifest for this the firmware build artifact.
21. Notice the following fields:

    1. **_type** => the URL where adherance to the metadata spec can be validated
    1. **name & digest** - for each artifact file  are generated by the SLSA attestation.
    1. **externalParameters** - only variable names are provided as the list of ones in play can be helpful, but exposing their data values may expose secrets. So all the blank values is an expected condition.
    1. **internParameters, runDetails and metadata** - near the bottom - all give information about the actual process invocation.

| ![Small Tanuki](images/admonitions/gitlab-logo-20.png) GitLab Game Changer: SLSA v1.0 Attestation |
| ------------------------------------------------------------ |
| With very little configuration the pipeline now has SLSA v1.0 artifact attestion which makes your process compliant with L1 of the standard. This is added on top of Sigstore certificateless code signing. |

| ![Checklist icon](images/admonitions/success-check-circle25.png)  Accomplished Outcomes |
| ------------------------------------------------------------ |
| - Performed Sigstore signing and signature verification of firmware binary<br />- Enable SLSA Attestation for Generated Artifacts |
