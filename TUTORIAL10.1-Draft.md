# DRAFT: Tutorial 6: Using NPM Packaging for Longterm Firmware Archive

## Known Working Version Details

Tested Date: **2024-05-30**

Testing Version (GitLab and Runner): **Enterprise Edition 17.0.0 SaaS**

To report problems, [check the issues here](https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-on-rpi-pico-w) to see if it is already known before creating a new issue in that project.

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
  **IMPORTANT**: Generally you are asked to close Web IDE tabs immediately after commiting because using old ones causes Merge Conflicts if the code has been updated in another Web IDE tab.

## Pre-Requisties

This tutorial requires that Tutorial 1 has been completed.

## Concepts To Watch For

- You will simply be doing a manual project by hand to set a baseline of a manual embedded development workflow.
- The exercises use C language rather than on-chip hobby languages and frameworks.

## Exercises

### 1.1 To Be Built 



