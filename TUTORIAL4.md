# Tutorial 4: Setup GitLab Runner Gateway to Flash Firmware

## Known Working Version Details

Tested Date: **2024-06-06**

Testing Version (GitLab and Runner): **Enterprise Edition 17.0.0 SaaS**

Tested Hardware: Raspberry Pi 3A+ 512MB (as "Big Pi"), Raspberry Pi 4b 4GB (as "Big Pi"), Raspberry Pi Pico

To report problems, [check the issues here](https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-on-rpi-pico-w) to see if it is already known before creating a new issue in that project.

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
  **IMPORTANT**: Generally you are asked to close Web IDE tabs immediately after commiting because using old ones causes Merge Conflicts if the code has been updated in another Web IDE tab.

## Prerequisties

This tutorial requires that Tutorial 1-3 have been completed.

| ![Checklist icon](images/admonitions/list-ul25.png)  Learning Objectives and Concepts |
| ------------------------------------------------------------ |
| - Configuration of GitLab Runner as a gateway to an embedded device.<br />- Steps required to re-engineer firmware flashing into CI<br />- Prepares for Hardware In the GitLab CI Loop<br />![labsetup](images/objectivesflashfromci.png) |

| ![DevOps Logo](images/admonitions/devops25.png)CI Automation Engineering: Requirements for Flashing Firmware |
| ------------------------------------------------------------ |
| - Install GitLab Runner onto the Big Pi to make it a Runner Gateway to the Pico<br />- Put the Pico in boot mode to receive code<br />- Identify where it's device attached to the Runner Gateway and Mount that Location<br />- Perform firmware flash<br />- Reset Pico to run mode |

| ![](images/admonitions/lightbulb25.png) Electronic Copy of Workshop Exercises |
| ------------------------------------------------------------ |
| Since there is a lot of code copying from these exercises, you may wish to open them in a dedicated browser window at:  https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-workshop-refactoring-to-ci |

## Exercises

### 4.1 Install GitLab Shell Runner on Big Pi to Create a Runner Gateway

This lab assumes you are running some version of Raspberry Pi OS, which is debian based. It follows the Runner setup instructions [here](https://docs.gitlab.com/runner/install/linux-repository.html#installing-gitlab-runner) with some additions.

1. SSH into your Big Pi.

2. Register the GitLab Debian Package repository with:
   `curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash`

3. Install the runner with:
   `sudo apt-get install gitlab-runner`

   | ![DevOps Logo](images/admonitions/danger-skull25.png) IMPORTANT |
   | ------------------------------------------------------------ |
   | It is important to use sudo with all runner commands as not using it **will not give errors**, but will give **incorrect information** instead because the runner command runs in your user context rather than a system-wide installation context. |

   Patience: It will take some time to download the large runner package and extract it - a long pause with the message `Unpacking gitlab-runner` is normal.

4. Check the runner status with:

   ```
   sudo gitlab-runner status
   ```

   ```
   Runtime platform                                    arch=arm64 os=linux pid=8992 revision=44feccdf version=17.0.0
   gitlab-runner: Service is running
   ```

   The above output indicates the runner service is active, but it is not registered to any group in GitLab.

5. Since we'll need hardware access to manage the Pico W board, configure gitlab-runner for passwordless SUDO with this command:

   ```
   echo "gitlab-runner ALL=(ALL) NOPASSWD: ALL" | sudo tee -a /etc/sudoers.d/gitlab-runner
   ```

6. In a web browser, use the GitLab UI to **navigate to** ‘full/path/to/yourgroup’ (the group that your project is in)

7. On the left navigation, **Select** 'Build => Runners'

8. On the upper right of the page, **Click** 'New group runner'

9. On the 'New group runner' page complete the following (leave any unmentioned form fields at their defaults):

   1. For **Tags** = 'pico-{5randomchars}' (If you have difficulty thinkin of 5 random characters, use https://www.random.org/strings/)
   2. For **Runner description** = 'Pico Gateway Runner for pico-{your5uniquechars}'

10. **Click** 'Create runner'

11. On the next page, Copy {the code snippet under step 1}

12. Open a local text file and paste the code to keep track of the token.

13. Switch to your SSH session to your Big Pi

14. After you paste the command, but before you run it, you must prepend it with `sudo ` and postpend it with `  --executor shell --non-interactive`, it should look something like this before pressing enter (except with a real token):

    `sudo gitlab-runner register --url https://gitlab.com  --token this-is-not-a-real-token --executor shell --non-interactive`

    Representative success output:
    ```
    Runtime platform                                    arch=arm64 os=linux pid=9185 revision=44feccdf version=17.0.0
    Running in system-mode.                            
                                                       
    Verifying runner... is valid                        runner=sszCyrgHm
    Runner registered successfully. Feel free to start it, but if it's running already the config should be automatically reloaded!
     
    Configuration (with the authentication token) was saved in "/etc/gitlab-runner/config.toml"
    ```

15. List the runners with 

    ```bash
    sudo gitlab-runner list
    ```

     Representative output:

     ```
        Runtime platform                                    arch=arm64 os=linux pid=9196 revision=44feccdf version=17.0.0
        Listing configured runners                          ConfigFile=/etc/gitlab-runner/config.toml
        sdv                                                 Executor=shell Token=this-is-not-a-real-token URL=https://gitlab.com
     ```
16. In a web browser, use the GitLab UI to **navigate to** ‘full/path/to/yourgroup’

17. On the left navigation, **Select** 'Build => Runners'

18. You should see your runner registered and it should say that it is online - simliar to the below screenshot (the "owner" should be your group name)
    ![](images/registeredrunneringl.png)

| ![Small Tanuki logo](images/admonitions/info-circle25.png) Maintained GitLab Runner Raspberry Pi Gateway Install Code |
| ------------------------------------------------------------ |
| A maintained, run from the web script for installing GitLab Runner on a Raspberry Pi as a Gateway is maintained in [GitLab Runner RPI Gateway for Embedded](https://gitlab.com/guided-explorations/embedded/gitlab-runner-rpi-gateway-for-embedded). |

### 4.2 Testing The Runner

We will now test that we can run jobs on this runner and that sudo works.

1. Recenter your location by browsing to `full/path/to/yourgroup/pico-examples` 

2. **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

3. In the upper right, **Locate** and **Click** 'Code' (button) and **Select** ‘Open in Web IDE’

   **Note**: A new browser tab opens with VS Code editing a copy of your project.

4. In the file navigation of the VS Code Web IDE, **Navigate to and Click** '.gitlab-ci.yml'

5. Right before `gitlab_release:` insert this code:

   ```yaml
   pico_flash:
     stage: .pre
     tags:
       - pico-xxxxx #update to your actual runner tag for your gateway
     script:
       - |
         sudo gitlab-runner list
   ```

6. Edit the value under `tags:` to match your runner tag that you created in the registration.

   | ![Small Tanuki logo](images/admonitions/examine-microscope25.png) Runner Tag Mapping and Concurrency |
   | ------------------------------------------------------------ |
   | Due to the runner tag setup in the registration, we must have at least this tag to route our job to our Runner Gateway. Each runner gateway will need a unique tag and is NOT set to run untagged jobs to help with ensuring only one job runs at a time and all from the same pipeline. The runner config.toml must also have the setting `concurrent = 1` which is the default. |

7. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines with a colored number badge)

8. In *Commit message*, **Type** 'Test runner gateway'

9. **Click** ‘Commit to '1-updates'

   > When working in GitLab’s integrated editing environments, creating a commit is also equivalent to doing a Git push operation, which will also trigger CI checks when CI is enabled.

10. **Close** the Web IDE tab.

11. From a web browser located at  `full/path/to/yourgroup/pico-examples` 

12. From the left navigation **Click** 'Build => Pipelines'

13. For the running pipeline, **Click** {the bubble in the Status column}

14. Wait for the pico_flash job to complete and in the upper right **Click** 'Cancel pipeline' (Note: it's also fine if the pipeline runs to completion)

15. **Click** 'pico_flash' (GitLab Job)

16. If everything is setup correctly, the runner registration token in the job should match the output of `sudo gitlab-runner list` when SSH'ed to your Big Pi.

### 4.3 Flash and Test Firmware Using GitLab CI

We will now download and flash the firmware as a test job before releasing.

1. From `full/path/to/yourgroup/pico-examples`, **Click** ‘Code => Merge requests => Resolve “Updates” ’ 

2. In the upper right, **Locate** and **Click** 'Code' (button) and **Select** ‘Open in Web IDE’

   **Note**: A new browser tab opens with VS Code editing a copy of your project.

3. In the file navigation of the VS Code Web IDE, **Navigate to and Click** '.gitlab-ci.yml'

4. While retaining the runner tag, peplace the job pico_flash code with the below code - which is simply a combination of the above code into a job.

   ![Small Tanuki logo](images/admonitions/warning-triangle25.png) Notice the stage name change from `.pre` to `hil_test`

   ![Small Tanuki logo](images/admonitions/warning-triangle25.png) You must retain the runner tag from the previous job.
   
   ![Small Tanuki logo](images/admonitions/warning-triangle25.png) picotool was created and placed in a path location on your Big Pi in Tutorial 1. If it is not present on the Big Pi in /usr/sbin, then follow [TUTORIAL 99.1](TUTORIAL99-APPENDICES.md) to build and place it.
   
   ```yaml
   pico_flash:
     stage: hil_test
     tags:
       - {pico-xxxxx} # set to your Runner Gateway Tag
     script:
       - |
         if [[ "${CI_SCRIPT_TRACE}" == "true" ]] || [[ -n "${CI_DEBUG_TRACE}" ]]; then
           echo "Debugging enabled"
           set -xv
         fi
         #Remember this is real hardware and changes persist - so idempotent code is even more important ;)
         echo "Downloading the firmware we just built."
         wget --header="JOB-TOKEN: $CI_JOB_TOKEN" -O ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${GitVersion_LegacySemVer}/${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz
         tar -xvf ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz
         echo "Flashing Pico"
         sudo picotool load -F picow_blink.uf2
         sudo picotool verify picow_blink.uf2
         sudo picotool reboot
   ```
   
5. Ensure the value under `tags:` to matches your runner tag.

6. Near the top of the .gitlab-ci.yml file, update the `stages:` line to include `hil_test` right before release, for a result like this:

   ```
   stages: [.pre, build, test, deploy, hil_test, release, .post]
   ```
   
8. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines with a colored number badge)

9. In *Commit message*, **Type** 'Test HIL'

10. **Click** ‘Commit to '1-updates'

   > When working in GitLab’s integrated editing environments, creating a commit is also equivalent to doing a Git push operation, which will also trigger CI checks when CI is enabled.

11. **Close** the Web IDE tab.

12. From a web browser located at  `full/path/to/yourgroup/pico-examples` 

13. From the left navigation **Click** 'Build => Pipelines'

14. For the running pipeline, **Click** {the bubble in the Status column}

15. Watch for the pipeline to stop at `pico_flash`

16. Physically observe the Pico LED to see if it is flashing at a slow rate.

    | ![Small Tanuki logo](images/admonitions/gitlab-logo-20.png) GitLab Game Changer: Extensive Workflow Control |
    | ------------------------------------------------------------ |
    | GitLab Manual jobs are a flexible way of interfacing automated pipelines with human workflow controls. They can be configured to only happen under certain conditions. They can be configured to require approvals and separately restricted as to who can press the play button (approvers can only approve, deployers can only push play). GitLab has many other workflow controls that are depicted as purple boxes in this illustration.<br />![workflow control visual](images/workflowcontrolpartslist.png) |

18. If your Pico "passed" the test and is flashing at a slow rate, then on the `gitlab_release` job, **Press** {the play icon} to officially release the firmware by creating a GitLab Release.

19. After the release job completes successfully, on the left navigation **Click** 'Deploy => Releases'

20. Near the bottom of the latest release object, look for the text "Released just now by {your avatar icon}"

| ![Checklist icon](images/admonitions/success-check-circle25.png)  Accomplished Outcomes |
| ------------------------------------------------------------ |
| - Added Hardware In the (CI) Loop by configuring GitLab Runner as a gateway to an embedded device<br />- Install GitLab Runner onto the Big Pi to make it a Runner Gateway to the Pico<br />- Performed firmware flash via GitLab CI |
