# Tutorial 6: Create a Pico W Device Cloud in GitLab and for Hardware in the CI Loop Testing

## Known Working Version Details

Tested Date: **2024-06-20**

Testing Version (GitLab and Runner): **Enterprise Edition 17.0.0 SaaS**

To report problems, [check the issues here](https://gitlab.com/guided-explorations/embedded/workshops/embedded-devops-on-rpi-pico-w) to see if it is already known before creating a new issue in that project.

## Conventions and Requirements

- Frequently a click in a browser application will launch a new tab. The exercise instructions will also frequently ask you to return to the tab that launched a new tab.  It can be challenging to keep your context when web applications open new tabs so the exercises generally highlight when a new tab has been opened. Keep this in mind as you work through and do not be quick to clean up open tabs.
  **IMPORTANT**: Generally you are asked to close Web IDE tabs immediately after commiting because using old ones causes Merge Conflicts if the code has been updated in another Web IDE tab.

## Pre-Requisties

This tutorial requires that Tutorials 1 - 4 have been completed.

| ![Checklist icon](images/admonitions/list-ul25.png)Learning Objectives and Concepts |
| ------------------------------------------------------------ |
| - A Device Cloud allows expensive hardware rigs to be shared virtually around the globe.<br />- The Device Cloud CI Component is a lightweight solution to this problem that is implemented 100% within GitLab with no external dependencies. |

## Exercises

### 6.1 Create A Project to Manage the Device Cloud Data

![](images/admonitions/classroom20.png) **For Classrooms**: If you are in a classroom session, the instructor will perform this exercise for the entire class. They may may do a walkthrough demo of what was done.

1. Pick a group in GitLab to house the Device Cloud Pool Data. If this is a class that has a common `classroom_group` that is a good place for it.

   | ![](images/admonitions/info-circle25.png)No Hierarchy Correlation Of This Project |
   | ------------------------------------------------------------ |
   | While it generally makes sense to locate this group in a place related to what it is managing, there is no requirement for it to be in any given GitLab group heirarchy in relationship to Groups where the runners are registered nor Groups or Projects that use the runners. Location is determined by the Project ID and permissions are given via a Project Access Token. |

2. Create a new project named "Device Cloud for RPI Pico W" (referenced as `device-cloud-for-rpi-pico-w` from here on)

3. Create a project access token to allow pipelines to set the runner allocations.

4. While in `device-cloud-for-rpi-pico-w`, from the left navigation, **Click** 'Settings => Access Tokens'

5. On the *Project Access Tokens* page, **Click** 'Add new token' and complete the following values:

   1. **Token name** = 'Device Cloud  for RPI Pico W Access'
   2. **Select a role** = 'Maintainer'
   3. **Select scopes** = 'api'

6. **Click** 'Create project access token'

7. Next to *{the masked token value}*, **Click** {the clipboard icon}

8. Open a parent group of your build project 
   ![](images/admonitions/classroom20.png) **For Classrooms**:  whatever you've used as the `classroom_group`

9. From the left navigation, **Click** 'Settings => CI/CD'

10. Next to *Variables*, **Click** 'Expand'

11. **Click** 'Add variable'

12. For *Visibility*, **Select** 'Masked'

13. **IMPORTANT:** For *Flags*, **Deselect** 'Protect variable'

14. For *Description*, **Type** 'Token for Device Cloud for RPI Pico 3'

15. For *Key*, **Type** 'DEVICE_POOL_TOKEN'

16. For *Value*, **Right Click the field and Select** 'Paste'

17. Near the bottom of the Add variable panel, **Click** 'Add variable'

    | ![](images/admonitions/info-circle25.png)Token and Variable  |
    | ------------------------------------------------------------ |
    | This variable:<br />- Is least privileged because the project only contains variables for HIL orchestration<br />- Should not be stored in GitLab CI YML where is would be revealed - but rather stored as a GitLab CI Variable on the Instance where it can be masked<br />- Must flow into all projects that need to use this specific Device Cloud POOL<br />- to more tightly scope who can use the HIL POOL you could issue individual Project Access Tokens for each project that uses the Device Cloud POOL. |

18. Switch back to the project `device-cloud-for-rpi-pico-w`

19. From the left navigation, **Click** 'Settings => General'

20. Near the top center of the page, **Copy** 'Project ID'

21. Open a parent group of your build project 
    ![](images/admonitions/classroom20.png) **For Classrooms**:  whatever you've used as the `classroom_group`

22. From the left navigation, **Click** 'Settings => CI/CD'

23. Next to *Variables*, **Click** 'Expand'

24. **Click** 'Add variable'

25. **IMPORTANT:** For *Flags*, **Deselect** 'Protect variable'

26. For *Description*, **Type** 'Token for Device Cloud for RPI Pico 3'

27. For *Key*, **Type** 'DEVICE_POOL_PROJECT_ID'

28. For *Value*, **Right Click the field and Select** 'Paste'

29. Near the bottom of the Add variable panel, **Click** 'Add variable'

    > All projects in the hierarchy under this project will use this device cloud by default (but individual projects can override the variables to use other device clouds)

### 6.2 Construct Command Line For Provisioning Into the Pool

![](images/admonitions/classroom20.png) **For Classrooms**: If you are in a classroom session, the instructor will perform this exercise for the entire class and provide the command line. They may may do a walkthrough demo of what was done.

1. In the GitLab.com CI/CD Catalog, find the Device Cloud Component at https://gitlab.com/explore/catalog/guided-explorations/embedded/ci-components/device-cloud

2. Under *Setting up the Device Cloud HIL Pool*, open the link to https://gitlab.com/guided-explorations/embedded/ci-components/device-cloud/-/blob/main/Device-Cloud-Pool-Mgmt/SETUP.md

3. From the section *Parameter Set 1: Full provisioning (minimum required parameters): Create Runner, Automatically Update HIL POOL DATA Project*, copy the oneliner code line and put it in a new vs code document (you won't be saving it, you just need a proper text editing environment to construct it)

   ```bash
   wget https://gitlab.com/guided-explorations/embedded/ci-components/device-cloud/-/raw/main/Device-Cloud-Pool-Mgmt/PROVISIONDEVICEGWRUNNER.sh?ref_type=heads -O /tmp/PROVISIONDEVICEGWRUNNER.sh ; sudo bash /tmp/PROVISIONDEVICEGWRUNNER.sh --runnergroupurl=https://gitlab.com/abccompany/embedded/ --runnergroupid=289127327 --runnergroupaccesstoken=glpat-faketokenwithcreaterunnerrights --devicepooldataprojectpath abccompany/devicepools/RPIPICOW  --devicepooldataprojectaccesstoken=faketokenwithapiandmaintainer
   ```

4. Replace the value in `--runnergroupurl=https://gitlab.com/abccompany/embedded/` with the full group path to the `classroom_group` - the url should be what is seen when you click into a group. It should not include the `/groups/` level.

5. While in the same group, **Click** 'Settings => General' and copy the Group ID and customize this part of the command line with it: `--runnergroupid=289127327`

6. While in the same group, create a Group Access Token for creating runners, **Click** 'Settings => Access Tokens'

   1. **Click** 'Add new token'
   2. Name the token PROVISIONING_FOR_RPIPICOW
   3. For *Select a role*, **Select** 'Owner'
   4. For *Scope*, **Select** 'create_runner' and 'manage_runner'
   5. Near the bottom, **Click** 'Create group access token'
   6. Next to {the masked token value}, **Click** {the clipboard icon}

7. In the command line, paste the token in for the value of `--runnergroupaccesstoken=glpat-faketokenwithcreaterunnerrights`

8. Browse to the project `device-cloud-for-rpi-pico-w`

9. Copy only the project path without the instance URL. Do not include '/groups/' if it exists and no leading or trailing slashes. Use this value to update `--devicepooldataprojectpath abccompany/devicepools/RPIPICOW`

10. In the `classroom_group`, from 'Settings => CI/CD => Variables' copy the value of DEVICE_POOL_TOKEN and update the value of `--devicepooldataprojectaccesstoken=faketokenwithapiandmaintainer`

11. At the very end of the command, add this parameter: `--

12. All the values in the template command line should now be replaced with real values - double check your updates.

### 6.3 Reconfigure Your CI Job to Use the Runner Gateway Pool

1. SSH into your Big Pi

2. Paste the oneliner you constructed.

   ![](images/admonitions/classroom20.png) **For Classrooms**: The instructor will share the oneliner for the classroom.

3. Ensure there are no errors in the installation.

4. Visit the group you used for runner creation and **Click** 'Build => Runners' to observe your new runner.
   ![](images/admonitions/classroom20.png) **For Classrooms**: The instructor may do this as a demo.

5. Visit the project you used for data (`device-cloud-for-rpi-pico-w`) and **Click** 'Settings => CI/CD => Variables' and observe the variable for allocating your runner (same name as the tag)

   ![](images/admonitions/classroom20.png) **For Classrooms**: The instructor may do this as a demo.

### 6.4 Convert Your Pipeline to use The Component and Device Cloud

1. While in a browser tab on `full/path/to/yourgroup/pico-examples`

2. **Click** ‘Plan => Issues => New issue’ (button)

3. On *New Issue*, for *Title (required)*, **Type** ‘Device Cloud’

4. **Click** ‘Create issue’

5. On *Updates {new issue view}*, **Click** ‘Create merge request’ 

6. On *New merge request*, **Uncheck** ‘Mark as draft’

7. **Click** ‘Create merge request’

8. On *Resolve “"New Feature" {new merge request view}*

9. Find the button *Code* and **Click** {its expand down arrow} and **Click** ‘Open in Web IDE’

10. In the left file navigation **Click** .gitlab-ci.yml

11. In the job pico_flash, change the `tags:` section to reference the variable `$DEVICE_SELECTED_ID` like this:

    ```yaml
      tags:
        - $DEVICE_SELECTED_ID
    ```

12. In the left hand file navigation, **Right-Click** .gitlab-ci.yml and **Select** 'Rename' 

14. **Type** 'device-pipeline.gitlab-ci.yml'

15. At the top of the file navigation, to the right of PICO_EXAMPLES, **Click** {the add document icon} (a little page with a plus on it)

16. The file name editable text opens, **Type** '.gitlab-ci.yml' (the leading period must be used, do not include the single quotes)

17. In **a NEW** browser tab, open the GitLab CI/CD Catalog (https://gitlab.com/explore/catalog)

18. **Search for and Click** 'GitLab CI On-Premise Device Cloud for Embedded Development' (https://gitlab.com/explore/catalog/guided-explorations/embedded/ci-components/device-cloud)

19. Near the top, in the Tab bar 'Readme | Components', **Click** 'Components'

20. In the code sample, **Click** {the clipboard icon}

21. Switch back to the Web IDE browser tab and **Paste the code** into .gitlab-ci.yml

22. It should look something like this:

    ```yaml
    include:  
    - component: $CI_SERVER_FQDN/guided-explorations/embedded/ci-components/device-cloud/device-cloud@0.1.124
    ```

23. **Click** 'device-pipeline.gitlab-ci.yml'

24. Find the `workflow:` section and use the editor to Cut it to the copy buffer. It should look like this:

    ```
    workflow:
      rules:
        - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG'
          when: never
        - if: $CI_COMMIT_BRANCH
          when: always
    ```

    

25. On the left file navigation,  **Click** .gitlab-ci.yml

26. Past the workflow rules so that the final file looks like this:

    ```
    include:  
    - component: $CI_SERVER_FQDN/guided-explorations/embedded/ci-components/device-cloud/device-cloud@0.1.124
    
    workflow:
      rules:
        - if: '$CI_PIPELINE_SOURCE == "push" && $CI_COMMIT_TAG'
          when: never
        - if: $CI_COMMIT_BRANCH
          when: always
    ```

27. Due to this issue [Keyless signing with sigstore not working in child pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/422146), we'll comment out cosign. Keep in mind you could implement this job outside of the device pipeline to retain the functionality.

28. In the `script:` section of the job `create_generic_package`, remove each occurance of the `cosign` command and the line adding the signature to `include.lst` and any echos that simply document cosign actions, the result should look like this:

    ```
      script:
        - |
          if [[ "${CI_SCRIPT_TRACE}" == "true" ]] || [[ -n "${CI_DEBUG_TRACE}" ]]; then
            echo "Debugging enabled"
            set -xv
          fi
          cd build/pico_w/wifi/blink
          echo "picow_blink.uf2" >> includefiles.lst
          tar -czvf ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz -T includefiles.lst
          echo "Pushing package to registry."  
          curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file ${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${CI_PROJECT_NAME}/${GitVersion_LegacySemVer}/${CI_PROJECT_NAME}.${GitVersion_LegacySemVer}.tar.gz
    ```

29. On {the far left icon navigation}, **Click** {the git icon} (three small circles connected by lines with a colored number badge)

30. In *Commit message*, **Type** 'Switch to device cloud and containers'

31. **Click** ‘Commit to '{number}-device-cloud'

32. **Close** the Web IDE tab.

33. **Click** {the Merge Request tab from which the IDE was launched}

34. If the MR Tab is no longer around, browse to `full/path/to/yourgroup/pico-examples`  and **Click** ‘Code => Merge requests => Resolve “Device Cloud” ’ 

35. Near the top, under the MR title, **Click** ‘Pipelines’ ( a tab)

36. **Click** {the running pipeline}

37. **Click** 'device-ci-allocate' (job bubble)

38. Find and make note of "DEVICE_SELECTED_ID"

39. ssh to your Big Pi

40. Enter the command `sudo cat /etc/gitlab-runner/devicepooldata.env`

41. Check if the value for RunnerCIVarStored matches DEVICE_SELECTED_ID

    > Note: If you are doing these labs alone, the values will match for sure - but your device is still being dynamically allocated and deallocated.



