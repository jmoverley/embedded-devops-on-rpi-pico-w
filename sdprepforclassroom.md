

https://github.com/gitbls/sdm/wiki/Batch-burn-SSD-SDs-with-sdm-gburn

```
#!/bin/bash
#
# Burn a bunch of SSDs/SD Cards from the same image
# Each card will have a unique, manual IP. 
# The last segment of the IP will be appended to the hostname, username and password to avoid 
# accidental cross-login by classroom participants

if [[ ! command -v sdm ]]; then 
  echo "Installing SDM"
  curl -L https://raw.githubusercontent.com/gitbls/sdm/master/EZsdmInstaller | bash
fi

STARTIP=100
ENDIP=125
WIFISSID=ABC
WIFIPWD=HARDTOGUESS
WIFICOUNTRY=US
CLASSCNETADDR=192.168.1       #Just the first 3 segments
ROUTERADDR=${CLASSCNETADDR}.1 #Fix if router is not at 1
DNSADDR=$ROUTER               # or 8.8.8.8
SDLISTFILE=SDLIST.CSV
BURNDEV="/dev/sdc" # Change this to be the name of the device you want to burn to
RPIIMG="/path/to/customized.img" # Change this to be the full path to the IMG you want to burn

if [[ -f $SDLISTFILE ]]; then echo "Deleting $SDLISTFILE"; rm -rf $SDLISTFILE ; fi
for hn in $(seq $STARTIP $ENDIP)
do
  echo "edb$hn password=pass$hn,wificountry=US,wifissid=$WIFISSID,wifipassword=$WIFIPWD,hostname=edb$hn,dhcpcd=[dev:eth0;ipaddress:$CLASSCNETADDR.$hn;router:$ROUTERADDR;dns:$DNSADDR]" | tee -a $SDLISTFILE
done

#/usr/local/sdm/sdm-gburn $RPIIMG $SDLISTFILE $BURNDEV
```