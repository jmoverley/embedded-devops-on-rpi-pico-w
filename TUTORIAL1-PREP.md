
[TOC]

## 1.1. Physical Connections

Check that all the physical connections fit together like this diagram (the Raspberry Pi Pico will not give any indicators of power)

**IMPORTANT**: Never place the board on any metal surface or touching any metal object as it can cause shorts and damage the board.

![Hardware Setup Graphic](images/preflighthardwareconfig.png)

## 1.2 Preparing the SD Card

![Small Tanuki](images/admonitions/classroom20.png) **For Classrooms**: Instructor Led Classrooms may provide an already prepared SD Card for you.

![Small Tanuki](images/admonitions/classroom20.png) **For Classroom Instructors**: As per the [Special Considerations For Classroom Environments](SELFPACED-INSTRUCTOR-DEMOER-Requirements.md) it is best to make each SD unique to prevent confusion by students while using SSH during calss.

Based on menu options for Raspberry Pi Imager v1.8.5

You will need to know your Wifi network logon details.

You can alternatively use a physical network connection if that works better.

You will also need to either:

- To verify that the network you are joining dynamically resolves .local host names. So an RPI host name of Mypi5 is frequently available at host name `mypi5.local`
- Or to be able to login to the router administration page to find the DHCP assigned ip address.

The [Raspberry Pi Imager Step-by-Step](https://www.raspberrypi.com/documentation/computers/getting-started.html#install-using-imager) instructions were the source for the below.

1. Insert the Micro-SD Card into your laptop. You may have to use a docking station or SD adapter to fit it into a media slot on attached to your laptop.

2. Download and install the appropriate Raspberry Pi Imager for your OS [from here](https://www.raspberrypi.com/software/).

3. Start the Raspberry Pi Imager.

4. For *Raspberry Pi Device*, **Select** the appropriate Raspberry Pi hardware for your Big Pi (NOT the Pico W)

5. For *Operating System*, avoid using a Desktop Environment OS, by **Selecting** 'Raspberry Pi OS (other)'

6. On the next page, **Select** **'Raspberry Pi OS Lite (64-bit)' (Debian Bookworm distro)**

   > Note that "Lite" indicates a CLI only environment without the overhead of graphical desktop OS.

7. For *Storage*, **Select** your MicroSD card. While hard drives generally do not show up, be sure to verify that the device is the right one. Verifying the size can help with knowing which one is correct. It may take a second for the card to appear. If there is difficulty showing the card for selection, try quitting and restarting Raspberry Pi Imager.

8. On the bottom right, **Click** 'Next'

9. On the *Use OS customization?* dialog, **Click** 'EDIT SETTINGS'

10. On the _OS Customization: GENERAL_ tab, complete the following information:

    1. hostname (write it down => we'll call this <<hostname>> from now on) (ALWAYS unique if you will be using multiple PIs on the same network in a classroom)
    2. username (write it down => we'll call this <<username>> from now on)
    3. password (write it down => we'll call this <<password>> from now on)
    4. Wi-Fi credentials
    5. Wifi LAN country (Important)
    6. the time zone
    7. your keyboard layout

11. On the _OS Customization: SERVICES_ tab, **Select** 'Enable SSH' and 'Use password authentication'

12. On the bottom of _OS Customization_, **Click** 'SAVE'

13. On the *Use OS customization?* dialog, **Click** 'YES'

14. On the _Warning_ dialog, **Click** 'YES'

15. You may be prompted for a password in order to overwrite the SD Card, provide your workstation password.

### 1.3 Booting the Big Pi

Hands on: 1m, wait: 10m

1. Insert the MicroSD Card into the Big Pi. The SD Card slot is the only port back of the Pi. The card will only insert one way (with the metal contacts facing toward from the board surface)

2. Attach a powered USB cable to the power port of the Big Pi (USB Micro-B or USB-C - NOT a USB-A port). (The bare board does not have sufficient current to shock you)

   ![Small Tanuki](images/admonitions/info-circle25.png) **First Boot:** The first boot of the Pi takes a while (3-5mins) as a bunch of one-time provisioning tasks are done.

### Lucky You if .local Name Resolution Works!

3. Attempt to ssh with one of these:

   ```bash
   ssh <<username>>@<<hostname>>.local #don't forget the '.local'
   ```

   If you do not have access to update your local `known_hosts` file, or you have to reburn the SD multiple times, the following command additions avoid the need to update known_hosts
   
   ```bash
   ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null <<username>>@<<hostname>>.local #don't forget the '.local'
   ```

### If .local Name Resolution Does Not Work, then

3. Login to the wireless router for the network that you specified during SD Card creation

4. Use the router settings menu settings to find the DHCP IP address most likely assigned to your Big Pi (this is router specific, but usually under some kind of "LAN" choice and then some kind of "DHCP" choice after that)

5. If you were given the IP address as part of a classroom, use it in the next steps.

6. Substitute your IPADDRESS and USERNAME in the following command from a terminal/command prompt on your laptop to connect to the Big Pi.
   The command avoids having to update your 

   ```bash
   ssh <<username>>@IPADDR
   ```

   If you do not have access to update your local `known_hosts` file, or you have to reburn the SD multiple times, the following command additions avoid the need to update known_hosts
   ```bash
   ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null <<username>>@IPADDR
   ```

 ## 1.4 Verifying Raspberry Pi Pico W Connection

1. Connect the Raspberry Pi Pico to the Big Pi with the USB A to USB Micro-B cable. Any USB A port on the Big Pi should work, but if you have troubles, try another of these ports.

2. Disconnect the Pico and press and hold the small white surface button near the usb connector while reconnecting it.

3. Plug one end of the USB A to USB-Micro-B cable into an USB port on the Big Pi.

4. Hold down the bootsel button on the Pico and plug it into the USB-Micro-B cable end.

5. Find which device the Pico attaches as with `sudo lsblk`
   Look for a lines like the below

   ```NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINTS
   sda           8:0    1  128M  0 disk
   └─sda1        8:1    1  128M  0 part
   mmcblk0     179:0    0 57.9G  0 disk
   ├─mmcblk0p1 179:1    0  512M  0 part /boot/firmware
   └─mmcblk0p2 179:2    0 57.4G  0 part /
   ```

   From this we know the device is sda1 (mmcblk0 is the raspberry pi SD card device)

6. Make a mount directory with ` sudo mkdir -p /mnt/pico`

7. Mount the device to the directory with (but substitute your actual found device first) `sudo mount /dev/sda1 /mnt/pico`

8. Check that we are actually mounted with the command (and observe the two files that list immediately after in order to verify connection)
   ```
   $ ls /mnt/pico/
   INDEX.HTM INFO_UF2.TXT

9. If you can see the above files you have completed verification of power and data accessibility of the Raspberry Pi Pico W.