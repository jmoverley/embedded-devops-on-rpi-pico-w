# Appendicies

### 99.1 Create and test picotool utility

| ![Small Tanuki logo](images/admonitions/gitlab-logo-20.png) CI Automation Engineering: On board code |
| ------------------------------------------------------------ |
| We will now create a utility on the Big Pi that is capable of switching the Big Pi to "boot select" mode without having to us the physical switch. The inclusion of pico_stdio_usb in our last firmware build was necessary for the Pico to respond to this new command. |

1. SSH to the Big Pi.

2. Perform the following steps to build the picotool utility and make it available to the shell runner jobs:

   ```bash
   cd /pico ;
   sudo apt install pkg-config libusb-1.0-0-dev -y ;
   git clone https://github.com/raspberrypi/picotool.git ;
   cd picotool ;
   mkdir build
   cd build
   cmake ..
   make
   sudo mv picotool /usr/sbin
   ```

   Be sure there is no errors in the command output from the above.

3. Check that the light on the Pico is still blinking. (It will go off if we are able to put it in bootsel mode.)

4. Run the new utility to put the Pico in bootsel mode like this:

   ```bash
   sudo picotool reboot -F -u
   ```

5. If the light goes off, our test is successful.

6. Run the new utility to put the Pico in application mode like this (`-u` is removed)

   ```bash
   sudo picotool reboot
   ```

7. The light should blink again.
